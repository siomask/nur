import {OrderedMap, Map} from 'immutable'
const _OrderedMap:any = OrderedMap;
const _Map:any = Map;
export function generateId() {
    return Date.now()
}

export function fbDatatoEntities(data:any, RecordModel = _Map) {
    return (new _OrderedMap(data)).mapEntries(([uid, value]:any) => (
        [uid, (new RecordModel(value)).set('uid', uid)]
    ))
}
export function calculateAge(birthday: any) { // birthday is a date
    birthday = new Date(birthday);
    var ageDifMs = Date.now() - birthday.getTime();
    var ageDate = new Date(ageDifMs); // miliseconds from epoch
    const year = Math.abs(ageDate.getUTCFullYear() - 1970);
    return isNaN(year) ? '' : year;
}
