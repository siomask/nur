import { API, appName } from '../../config';
import { call, put } from 'redux-saga/effects';
import axios from 'axios';
import { createSelector } from 'reselect';
import { moduleName } from './config';
import { Record } from 'immutable';
import { Details } from './extra';


const _Record: any = Record;


export const IncidentDETAILSRecord: any = new _Record(new Details());
export const _moduleName = moduleName;
export const LOAD_ALL_Incident_DETAILS = `${appName}/${moduleName}/LOAD_ALL_Incidents_DETAILS`;
export const LOAD_ALL_Incidents_REQUEST_DETAILS = `${appName}/${moduleName}/LOAD_ALL_Incidents_REQUEST_DETAILS`;
export const LOAD_ALL_Incidents_SUCCESS_DETAILS = `${appName}/${moduleName}/LOAD_ALL_Incidents_SUCCESS_DETAILS`;
export const LOAD_ALL_Incidents_ERROR_DETAILS = `${appName}/${moduleName}/LOAD_ALL_Incidents_ERROR_DETAILS`;


export const stateSelector = (state: any) => state[moduleName];
export const IncidentsDETAILSSelected = createSelector(stateSelector, state => state.details);

export const incidentsDETAILSSelector = createSelector(IncidentsDETAILSSelected, (items) => {
  return items.valueSeq().toArray();
});

export function incidentsDETAILS(payload: any) {
  return {
    type: LOAD_ALL_Incident_DETAILS,
    payload,
  };
}


export const incidentsDETAILSSaga = function* ({ payload }: any) {
  try {
    yield put({
      type: LOAD_ALL_Incidents_REQUEST_DETAILS,
    });
    const res = yield call(() => {
        return axios.get(`${API}api/modules/EPIDEMIOLOGY/incidents/${payload.CaseNum}/details`);
      },
    );
    yield put({
      type: LOAD_ALL_Incidents_SUCCESS_DETAILS,
      payload: res.data.map((el: any, index: number) => ({ ...el, tempId: index })),
    });

  } catch (error) {
    yield put({
      type: LOAD_ALL_Incidents_ERROR_DETAILS,
      error,
    });
  }
};

