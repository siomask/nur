import { API, appName } from '../../config';
import { Record } from 'immutable';
import { all, cps, call, put, take, takeEvery } from 'redux-saga/effects';
import axios from 'axios';
import { createSelector } from 'reselect';
import { arrayToMap } from '../../utils';
import { moduleName } from './config';
import * as TIME_LINE from './timeline';
import * as HISTORY from './history';
import * as DETAILS from './details';
import * as STATUS from './status';
import * as BACTERIA from './bacteria';
import * as INCIDENT from './incident';
import { SIGN_OUT_SUCCESS } from '../auth';
import { Incident } from './extra';


const _Record: any = Record;
const IncidentsRecord: any = new _Record(new Incident());

export const ReducerRecord = Record({
  loading: false,
  error: null,

  incidentList: Date.now(),
  status: arrayToMap([], STATUS.IncidentStatusRecord),
  bacteria: arrayToMap([], BACTERIA.IncidentBACTERIARecord),

  details: arrayToMap([], DETAILS.IncidentDETAILSRecord),
  history: arrayToMap([], HISTORY.IncidentHistoryRecord),
  incidents: arrayToMap([], IncidentsRecord),
  timeLines: arrayToMap([], TIME_LINE.IncidentTiemLineRecord),
});

export const _moduleName = moduleName;
export const LOAD_ALL_Incidents = `${appName}/${moduleName}/LOAD_ALL_Incidents`;
export const LOAD_ALL_Incidents_REQUEST = `${appName}/${moduleName}/LOAD_ALL_Incidents_REQUEST`;
export const LOAD_ALL_Incidents_SUCCESS = `${appName}/${moduleName}/LOAD_ALL_Incidents_SUCCESS`;
export const LOAD_ALL_Incidents_ERROR = `${appName}/${moduleName}/LOAD_ALL_Incidents_ERROR`;

export const EDIT_COMMENT_Incidents = `${appName}/${moduleName}/EDIT_COMMENT_Incidents`;
export const EDIT_COMMENT_Incidents_REQUEST = `${appName}/${moduleName}/EDIT_COMMENT_Incidents_REQUEST`;
export const EDIT_COMMENT_Incidents_SUCCESS = `${appName}/${moduleName}/EDIT_COMMENT_Incidents_SUCCESS`;
export const EDIT_COMMENT_Incidents_ERROR = `${appName}/${moduleName}/EDIT_COMMENT_Incidents_ERROR`;


export default function reducer(state = new ReducerRecord(), action: any) {
  const { type, payload, error } = action;

  switch (type) {
    case SIGN_OUT_SUCCESS: {
      return new ReducerRecord();
    }

    case STATUS.LOAD_ALL_STATUS_REQUEST:
    case BACTERIA.LOAD_ALL_BACTERIA_REQUEST:
    case DETAILS.LOAD_ALL_Incidents_REQUEST_DETAILS:
    case HISTORY.LOAD_ALL_Incidents_REQUEST_HISTORY:
    case TIME_LINE.LOAD_ALL_Incidents_REQUEST_TIMELINE:
    case EDIT_COMMENT_Incidents_REQUEST:
    case LOAD_ALL_Incidents_REQUEST:
      return state.set('loading', true);


    case BACTERIA.LOAD_ALL_BACTERIA_SUCCESS: {
      return state
        .set('loading', false)
        .set('bacteria', arrayToMap(payload, BACTERIA.IncidentBACTERIARecord, 'EIBacteriaID'))
        .set('error', null);
    }
    case INCIDENT.CREATE_INCIDENT_SUCCESS: {
      return state
        .set('loading', false)
        .set('incidentList', Date.now())
        .set('error', null);
    }
    case STATUS.LOAD_ALL_STATUS_SUCCESS: {
      return state
        .set('loading', false)
        .set('status', arrayToMap(payload, STATUS.IncidentStatusRecord, 'EIStatusID'))
        .set('error', null);
    }
    case TIME_LINE.LOAD_ALL_Incidents_SUCCESS_TIMELINE: {
      const oldList = state.timeLines.valueSeq().toArray().map((el: any) => el.toObject());
      for (let i = 0; i < payload.length; i++) {
        let shouldAdd = true;
        for (let j = 0; j < oldList.length; j++) {
          if (oldList[j].EventHistoryID === payload[i].EventHistoryID) {
            oldList[j] = payload[i];
            shouldAdd = false;
            break;
          }
        }
        if (shouldAdd) {
          oldList.push(payload[i]);
        }
      }
      return state
        .set('loading', false)
        .set('timeLines', arrayToMap(oldList, TIME_LINE.IncidentTiemLineRecord, 'EventHistoryID'))
        .set('error', null);
    }
    case HISTORY.LOAD_ALL_Incidents_SUCCESS_HISTORY: {
      return state
        .set('loading', false)
        .set('history', arrayToMap(payload, HISTORY.IncidentHistoryRecord, 'EIID'))
        .set('error', null);
    }
    case DETAILS.LOAD_ALL_Incidents_SUCCESS_DETAILS: {
      return state
        .set('loading', false)
        .set('details', arrayToMap(payload, DETAILS.IncidentDETAILSRecord, 'tempId'))
        .set('error', null);
    }
    case LOAD_ALL_Incidents_SUCCESS: {
      return state
        .set('loading', false)
        .set('incidents', arrayToMap(action.payload, IncidentsRecord, 'EIID'))
        .set('error', null);
    }
    case EDIT_COMMENT_Incidents_SUCCESS: {
      const oldList = state.incidents.valueSeq().toArray().map((el: any) => el.toObject());
      for (let i = 0; i < oldList.length; i++) {
        if (oldList[i].EIID === action.payload.EIID) {
          Object.assign(oldList[i], action.payload);
          break;
        }
      }
      return state
        .set('loading', false)
        .set('incidents', arrayToMap(oldList, IncidentsRecord, 'EIID'))
        .set('error', null);
    }


    case DETAILS.LOAD_ALL_Incidents_ERROR_DETAILS:
    case HISTORY.LOAD_ALL_Incidents_ERROR_HISTORY:
    case TIME_LINE.LOAD_ALL_Incidents_ERROR_TIMELINE:
    case EDIT_COMMENT_Incidents_ERROR:
    case LOAD_ALL_Incidents_ERROR: {
      return state
        .set('loading', false)
        .set('error', error);
    }


    default:
      return state;
  }
}


export const stateSelector = (state: any) => state[moduleName];
export const IncidentsSelected = createSelector(stateSelector, state => state.incidents);

export const incidents = createSelector(IncidentsSelected, (items) => {
  return items.valueSeq().toArray();
});

export function editIncidentsComment(payload: any) {
  return {
    type: EDIT_COMMENT_Incidents,
    payload,
  };
}

export function loadAllIncidents() {
  return {
    type: LOAD_ALL_Incidents,
  };
}


const loadAllIncidentsSaga = function* () {
  try {
    yield put({
      type: LOAD_ALL_Incidents_REQUEST,
    });
    const res = yield call(() => {
        return axios.get(`${API}api/modules/EPIDEMIOLOGY/incidents`);
      },
    );
    yield put({
      type: LOAD_ALL_Incidents_SUCCESS,
      payload: res.data.map((el: any, index: number) => ({ ...el, tempId: index })),
    });

  } catch (error) {
    yield put({
      type: LOAD_ALL_Incidents_ERROR,
      error,
    });
  }
};
const editIncidentsCommentSage = function* ({ payload }: any) {
  try {
    yield put({
      type: EDIT_COMMENT_Incidents_REQUEST,
    });
    const res = yield call(() => {
        return axios.post(`${API}api/modules/EPIDEMIOLOGY/incidents/${payload.EIID}/comments`, payload);
      },
    );
    yield put({
      type: EDIT_COMMENT_Incidents_SUCCESS,
      payload: res.data,
    });

  } catch (error) {
    yield put({
      type: EDIT_COMMENT_Incidents_ERROR,
      error,
    });
  }
};

export const saga = function* () {
  yield all([
    takeEvery(LOAD_ALL_Incidents, loadAllIncidentsSaga),
    takeEvery(EDIT_COMMENT_Incidents, editIncidentsCommentSage),
    takeEvery(TIME_LINE.LOAD_ALL_Incident_TIMELINE, TIME_LINE.incidentsTimelineSaga),
    takeEvery(HISTORY.LOAD_ALL_Incident_HISTORY, HISTORY.incidentsHISTORYSaga),
    takeEvery(DETAILS.LOAD_ALL_Incident_DETAILS, DETAILS.incidentsDETAILSSaga),
    takeEvery(BACTERIA.LOAD_ALL_BACTERIA, BACTERIA.incidentsBACTERIASaga),
    takeEvery(STATUS.LOAD_ALL_STATUS, STATUS.incidentsStatusSaga),
    takeEvery(INCIDENT.CREATE_INCIDENT, INCIDENT.addIncidentSaga),
  ]);
};
