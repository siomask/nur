import { API, appName } from '../../config';
import { call, put } from 'redux-saga/effects';
import axios from 'axios';
import { createSelector } from 'reselect';
import { moduleName } from './config';
import { Record } from 'immutable';
import { Incident } from './extra';


const _Record: any = Record;


export const IncidentHistoryRecord: any = new _Record(new Incident());
export const _moduleName = moduleName;
export const LOAD_ALL_Incident_HISTORY = `${appName}/${moduleName}/LOAD_ALL_Incidents_HISTORY`;
export const LOAD_ALL_Incidents_REQUEST_HISTORY = `${appName}/${moduleName}/LOAD_ALL_Incidents_REQUEST_HISTORY`;
export const LOAD_ALL_Incidents_SUCCESS_HISTORY = `${appName}/${moduleName}/LOAD_ALL_Incidents_SUCCESS_HISTORY`;
export const LOAD_ALL_Incidents_ERROR_HISTORY = `${appName}/${moduleName}/LOAD_ALL_Incidents_ERROR_HISTORY`;


export const stateSelector = (state: any) => state[moduleName];
export const IncidentsHISTORYSelected = createSelector(stateSelector, state => state.history);

export const incidentsHISTORYSelector = createSelector(IncidentsHISTORYSelected, (items) => {
  return items.valueSeq().toArray();
});

export function incidentsHISTORY(payload: any) {
  return {
    type: LOAD_ALL_Incident_HISTORY,
    payload,
  };
}


export const incidentsHISTORYSaga = function* ({ payload }: any) {
  try {
    yield put({
      type: LOAD_ALL_Incidents_REQUEST_HISTORY,
    });
    const res = yield call(() => {
        return axios.get(`${API}api/modules/EPIDEMIOLOGY/incidents/${payload.CaseNum}/${payload.PatientID}/history`);
      },
    );
    yield put({
      type: LOAD_ALL_Incidents_SUCCESS_HISTORY,
      payload: res.data.map((el: any, index: number) => ({ ...el, tempId: index })),
    });

  } catch (error) {
    yield put({
      type: LOAD_ALL_Incidents_ERROR_HISTORY,
      error,
    });
  }
};

