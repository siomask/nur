import { API, appName } from '../../config';
import { call, put } from 'redux-saga/effects';
import axios from 'axios';
import { createSelector } from 'reselect';
import { moduleName } from './config';
import { Record } from 'immutable';


const _Record: any = Record;

export class Status {
  'EIStatusID': number;
  'Status': string;

  constructor(data: any = {}) {
    this.Status = data.Status;
    this.EIStatusID = data.EIStatusID;
  }
}

export const IncidentStatusRecord: any = new _Record(new Status());
export const _moduleName = moduleName;
export const LOAD_ALL_STATUS = `${appName}/${moduleName}/LOAD_ALL_STATUS`;
export const LOAD_ALL_STATUS_REQUEST = `${appName}/${moduleName}/LOAD_ALL_STATUS_REQUEST`;
export const LOAD_ALL_STATUS_SUCCESS = `${appName}/${moduleName}/LOAD_ALL_STATUS_SUCCESS`;
export const LOAD_ALL_STATUS_ERROR = `${appName}/${moduleName}/LOAD_ALL_STATUS_ERROR`;


export const stateSelector = (state: any) => state[moduleName];
export const IncidentsStatusSelected = createSelector(stateSelector, state => state.status);

export const incidentsStatusSelector = createSelector(IncidentsStatusSelected, (items) => {
  return items.valueSeq().toArray();
});

export function incidentsStatus(payload: any) {
  return {
    type: LOAD_ALL_STATUS,
    payload,
  };
}


export const incidentsStatusSaga = function* ({ payload }: any) {
  try {
    yield put({
      type: LOAD_ALL_STATUS_REQUEST,
    });
    const res = yield call(() => {
        return axios.get(`${API}api/modules/EPIDEMIOLOGY/incidents/status`);
      },
    );
    yield put({
      type: LOAD_ALL_STATUS_SUCCESS,
      payload: res.data.map((el: any, index: number) => ({ ...el, tempId: index })),
    });

  } catch (error) {
    yield put({
      type: LOAD_ALL_STATUS_ERROR,
      error,
    });
  }
};

