export class Incident {

  'EIID': number;
  'CaseNum': number;
  'Tasks': string;
  'LabResults': string;
  'InsertDate': string;
  'PatientID': number;
  'PatientName': string;
  'DepartmentAtAcquire': string;
  'BacteriaID': number;
  'EIStatusID': number;
  'Comments': string;
  'Priority': string;
  'OpenedBy': string;
  'EIBacteriaID': number;
  'BacteriaName': string;
  'Status': string;

  constructor(data: any = {}) {
    Object.assign(this, data);
  }
}
export class Details {

  'EventData': string;

  constructor(data: any = {}) {
    Object.assign(this, data);
  }
}

