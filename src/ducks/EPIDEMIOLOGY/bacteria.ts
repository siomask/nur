import { API, appName } from '../../config';
import { call, put } from 'redux-saga/effects';
import axios from 'axios';
import { createSelector } from 'reselect';
import { moduleName } from './config';
import { Record } from 'immutable';


const _Record: any = Record;

export class BACTERIA {
  'EIBacteriaID': number;
  'BacteriaName': string;

  constructor(data: any = {}) {
    this.EIBacteriaID = data.EIBacteriaID;
    this.BacteriaName = data.BacteriaName;
  }
}

export const IncidentBACTERIARecord: any = new _Record(new BACTERIA());
export const _moduleName = moduleName;
export const LOAD_ALL_BACTERIA = `${appName}/${moduleName}/LOAD_ALL_BACTERIA`;
export const LOAD_ALL_BACTERIA_REQUEST = `${appName}/${moduleName}/LOAD_ALL_BACTERIA_REQUEST`;
export const LOAD_ALL_BACTERIA_SUCCESS = `${appName}/${moduleName}/LOAD_ALL_BACTERIA_SUCCESS`;
export const LOAD_ALL_BACTERIA_ERROR = `${appName}/${moduleName}/LOAD_ALL_BACTERIA_ERROR`;


export const stateSelector = (state: any) => state[moduleName];
export const IncidentsBACTERIASelected = createSelector(stateSelector, state => state.bacteria);

export const incidentsBACTERIASelector = createSelector(IncidentsBACTERIASelected, (items) => {
  return items.valueSeq().toArray();
});

export function incidentsBACTERIA(payload: any) {
  return {
    type: LOAD_ALL_BACTERIA,
    payload,
  };
}


export const incidentsBACTERIASaga = function* ({ payload }: any) {
  try {
    yield put({
      type: LOAD_ALL_BACTERIA_REQUEST,
    });
    const res = yield call(() => {
        return axios.get(`${API}api/modules/EPIDEMIOLOGY/incidents/BACTERIA`);
      },
    );
    yield put({
      type: LOAD_ALL_BACTERIA_SUCCESS,
      payload: res.data.map((el: any, index: number) => ({ ...el, tempId: index })),
    });

  } catch (error) {
    yield put({
      type: LOAD_ALL_BACTERIA_ERROR,
      error,
    });
  }
};

