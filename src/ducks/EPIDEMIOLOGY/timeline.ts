import { API, appName } from '../../config';
import { call, put } from 'redux-saga/effects';
import axios from 'axios';
import { createSelector } from 'reselect';
import { moduleName } from './config';
import { Record } from 'immutable';


const _Record: any = Record;

export class TimeLine {
  'EventHistoryID': number;
  'CaseNum': number;
  'EventDate': string;
  'PatientID': string;
  'EventTypeID': number;
  'EV_Description': string;
  'EV_EventName': string;
  'EventData': string;

  constructor(data: any = {}) {
    this.EV_Description = data.EV_Description;
    this.EV_EventName = data.EV_EventName;
    this.EventHistoryID = data.EventHistoryID;
    this.CaseNum = data.CaseNum;
    this.EventDate = data.EventDate;
    this.PatientID = data.PatientID;
    this.EventTypeID = data.EventTypeID;
    this.EventData = data.EventData;
  }
}

export const IncidentTiemLineRecord: any = new _Record(new TimeLine());
export const _moduleName = moduleName;
export const LOAD_ALL_Incident_TIMELINE = `${appName}/${moduleName}/LOAD_ALL_Incidents_TIMELINE`;
export const LOAD_ALL_Incidents_REQUEST_TIMELINE = `${appName}/${moduleName}/LOAD_ALL_Incidents_REQUEST_TIMELINE`;
export const LOAD_ALL_Incidents_SUCCESS_TIMELINE = `${appName}/${moduleName}/LOAD_ALL_Incidents_SUCCESS_TIMELINE`;
export const LOAD_ALL_Incidents_ERROR_TIMELINE = `${appName}/${moduleName}/LOAD_ALL_Incidents_ERROR_TIMELINE`;


export const stateSelector = (state: any) => state[moduleName];
export const IncidentsTimeLineSelected = createSelector(stateSelector, state => state.timeLines);

export const incidentsTimeLineSelector = createSelector(IncidentsTimeLineSelected, (items) => {
  return items.valueSeq().toArray();
});

export function incidentsTimeline(payload: any) {
  return {
    type: LOAD_ALL_Incident_TIMELINE,
    payload,
  };
}


export const incidentsTimelineSaga = function* ({ payload }: any) {
  try {
    yield put({
      type: LOAD_ALL_Incidents_REQUEST_TIMELINE,
    });
    const res = yield call(() => {
        return axios.get(`${API}api/modules/EPIDEMIOLOGY/incidents/${payload.CaseNum}/timeline`);
      },
    );
    yield put({
      type: LOAD_ALL_Incidents_SUCCESS_TIMELINE,
      payload: res.data.map((el: any, index: number) => ({ ...el, tempId: index })),
    });

  } catch (error) {
    yield put({
      type: LOAD_ALL_Incidents_ERROR_TIMELINE,
      error,
    });
  }
};

