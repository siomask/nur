import { API, appName } from '../../config';
import { call, put } from 'redux-saga/effects';
import axios from 'axios';
import { createSelector } from 'reselect';
import { moduleName } from './config';
import { Record } from 'immutable';



export const _moduleName = moduleName;
export const CREATE_INCIDENT = `${appName}/${moduleName}/CREATE_INCIDENT`;
export const CREATE_INCIDENT_REQUEST = `${appName}/${moduleName}/CREATE_INCIDENT_REQUEST`;
export const CREATE_INCIDENT_SUCCESS = `${appName}/${moduleName}/CREATE_INCIDENT_SUCCESS`;
export const CREATE_INCIDENT_ERROR = `${appName}/${moduleName}/CREATE_INCIDENT_ERROR`;



export function addIncident(payload: any) {
  return {
    type: CREATE_INCIDENT,
    payload,
  };
}


export const addIncidentSaga = function* ({ payload }: any) {
  try {
    yield put({
      type: CREATE_INCIDENT_REQUEST,
    });
    const res = yield call(() => {
        return axios.post(`${API}api/modules/EPIDEMIOLOGY/incidents`,payload);
      },
    );
    yield put({
      type: CREATE_INCIDENT_SUCCESS,
      payload: res.data.map((el: any, index: number) => ({ ...el, tempId: index })),
    });

  } catch (error) {
    yield put({
      type: CREATE_INCIDENT_ERROR,
      error,
    });
  }
};

