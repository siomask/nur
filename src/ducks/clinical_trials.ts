import { API, appName } from '../config';
import { Record } from 'immutable';
import { all, cps, call, put, take, takeEvery } from 'redux-saga/effects';
import axios from 'axios';
import { createSelector } from 'reselect';
import { arrayToMap } from '../utils';
import { SIGN_OUT_SUCCESS } from './auth';


const _Record: any = Record;
const ClinicalTrialRecord: any = new _Record({
  ClinicalTrialID: null,
  Name: null,
  CountEnrolled: null,
  CountCandidates: null,
  ClinicalTrialDoc: new _Record({
    NCT: null,
    Status: null,
    EnrollementEstimationHealthInstitute: null,
  }),
});
const ClinicalTrialMemberDetailRecord: any = new _Record({
  id: null,
  ClinicalTrialID: null,
  Comments: null,
  ConditionID: null,
  Direction: null,
  Display: null,
  LastUpdate: null,
  OriginalText: null,
  Status: null,
  UpdateType: null,
  UpdatedBy: null,
  Value: null,
});

export const ReducerRecord = Record({
  loading: false,
  error: null,

  items: arrayToMap([], ClinicalTrialRecord),
  clinicalMemberDetails: arrayToMap([], ClinicalTrialMemberDetailRecord),
  trial: {
    tabs: [],
    patients: [],
  },
  watchlist: [],
});

export const moduleName = 'clinical_trials';
export const LOAD_ALL = `${appName}/${moduleName}/LOAD_ALL`;
export const LOAD_ALL_REQUEST = `${appName}/${moduleName}/LOAD_ALL_REQUEST`;
export const LOAD_ALL_SUCCESS = `${appName}/${moduleName}/LOAD_ALL_SUCCESS`;
export const LOAD_ALL_ERROR = `${appName}/${moduleName}/LOAD_ALL_ERROR`;
export const LOAD_TRIAL_MEMEBER = `${appName}/${moduleName}/LOAD_TRIAL_MEMEBER`;
export const LOAD_TRIAL_MEMEBER_REQUEST = `${appName}/${moduleName}/LOAD_TRIAL_MEMEBER_REQUEST`;
export const LOAD_TRIAL_MEMEBER_SUCCESS = `${appName}/${moduleName}/LOAD_TRIAL_MEMEBER_SUCCESS`;
export const LOAD_TRIAL_MEMEBER_ERROR = `${appName}/${moduleName}/LOAD_TRIAL_MEMEBER_ERROR`;
export const LOAD_TRIAL_MEMEBER_DETAILS = `${appName}/${moduleName}/LOAD_TRIAL_MEMEBER_DETAILS`;
export const LOAD_TRIAL_MEMEBER_DETAILS_REQUEST = `${appName}/${moduleName}/LOAD_TRIAL_MEMEBER_DETAILS_REQUEST`;
export const LOAD_TRIAL_MEMEBER_DETAILS_SUCCESS = `${appName}/${moduleName}/LOAD_TRIAL_MEMEBER_DETAILS_SUCCESS`;
export const LOAD_TRIAL_MEMEBER_DETAILS_ERROR = `${appName}/${moduleName}/LOAD_TRIAL_MEMEBER_DETAILS_ERROR`;
export const UPDATE_TRIAL_STATUS = `${appName}/${moduleName}/UPDATE_TRIAL_STATUS`;
export const UPDATE_TRIAL_STATUS_REQUEST = `${appName}/${moduleName}/UPDATE_TRIAL_STATUS_REQUEST`;
export const UPDATE_TRIAL_STATUS_SUCCESS = `${appName}/${moduleName}/UPDATE_TRIAL_STATUS_SUCCESS`;
export const UPDATE_TRIAL_STATUS_ERROR = `${appName}/${moduleName}/UPDATE_TRIAL_STATUS_ERROR`;


export default function reducer(state = new ReducerRecord(), action: any) {
  const { type, payload, error } = action;

  switch (type) {
    case UPDATE_TRIAL_STATUS_REQUEST:
    case LOAD_TRIAL_MEMEBER_DETAILS_REQUEST:
    case LOAD_TRIAL_MEMEBER_REQUEST:
    case LOAD_ALL_REQUEST:
      return state.set('loading', true);

    case LOAD_ALL_SUCCESS:
      return state
        .set('loading', false)
        .set('items', arrayToMap(action.payload.items, ClinicalTrialRecord))
        .set('error', null);

    case LOAD_TRIAL_MEMEBER_SUCCESS:
      return state
        .set('loading', false)
        .set('trial', payload.trial)
        .set('error', null);

    case UPDATE_TRIAL_STATUS_SUCCESS: {
      debugger;
      return state
        .set('loading', false)
        .mergeIn(['clinicalMemberDetails', payload.id], payload)
        .set('error', null);
    }


    case LOAD_TRIAL_MEMEBER_DETAILS_SUCCESS:
      return state
        .set('loading', false)
        .set('clinicalMemberDetails', arrayToMap(
          action.payload.data.map((el: any, index: any) => {
            el.id = index;
            return el;
          }),
          ClinicalTrialMemberDetailRecord,
          'id',
        ))
        .set('error', null);

    case UPDATE_TRIAL_STATUS_ERROR:
    case LOAD_TRIAL_MEMEBER_DETAILS_ERROR:
    case LOAD_TRIAL_MEMEBER_ERROR:
    case LOAD_ALL_ERROR: {
      return state
        .set('loading', false)
        .set('error', error);
    }


    case SIGN_OUT_SUCCESS:{
      return new ReducerRecord();
    }
    default:
      return state;
  }
}


export const stateSelector = (state: any) => state[moduleName];
export const itemsSelected = createSelector(stateSelector, state => state.items);
export const clinicalMemberDetailsSelector = createSelector(stateSelector, state => state.clinicalMemberDetails);
export const trialSelector = createSelector(stateSelector, state => state.trial);


export const trails = createSelector(itemsSelected, (items) => {
  return items.valueSeq().toArray();
});
export const clinicalMemberDetails = createSelector(clinicalMemberDetailsSelector, (items) => {
  return items.valueSeq().toArray();
});

export const trial = createSelector(trialSelector, (item) => {
  return item;
});


export function loadAll() {
  return {
    type: LOAD_ALL,
  };
}

export function loadTrialMembers(ClinicalTrialID: any) {
  return {
    type: LOAD_TRIAL_MEMEBER,
    payload: {
      ClinicalTrialID,
    },
  };
}

export function loadTrialMemberDetails(ClinicalTrialID: any, MemberID: any) {
  return {
    type: LOAD_TRIAL_MEMEBER_DETAILS,
    payload: {
      MemberID,
      ClinicalTrialID,
    },
  };
}

export function updateTrialStatus(payload: any) {
  return {
    type: UPDATE_TRIAL_STATUS,
    payload,
  };
}


const loadAllSaga = function* () {
  try {
    yield put({
      type: LOAD_ALL_REQUEST,
    });
    const res = yield call(() => {
        return axios.get(`${API}api/modules/clinical_trial`);
      },
    );
    yield put({
      type: LOAD_ALL_SUCCESS,
      payload: res.data,
    });

  } catch (error) {
    yield put({
      type: LOAD_ALL_ERROR,
      error,
    });
  }
};
const loadTrialMembersSaga = function* (action: any) {
  try {
    yield put({
      type: LOAD_TRIAL_MEMEBER_REQUEST,
    });
    const res = yield call(() => {
        return axios.get(`${API}api/modules/clinical_trial/${action.payload.ClinicalTrialID}/trial`);
      },
    );
    yield put({
      type: LOAD_TRIAL_MEMEBER_SUCCESS,
      payload: res.data,
    });

  } catch (error) {
    yield put({
      type: LOAD_TRIAL_MEMEBER_ERROR,
      error,
    });
  }
};
const loadTrialMemberDetailsSaga = function* ({ payload: { ClinicalTrialID, MemberID } }: any) {
  try {
    yield put({
      type: LOAD_TRIAL_MEMEBER_DETAILS_REQUEST,
    });
    const res = yield call(() => {
        return axios.get(`${API}api/modules/clinical_trial/${ClinicalTrialID}/patient/${MemberID}`);
      },
    );
    yield put({
      type: LOAD_TRIAL_MEMEBER_DETAILS_SUCCESS,
      payload: res.data,
    });

  } catch (error) {
    yield put({
      type: LOAD_TRIAL_MEMEBER_DETAILS_ERROR,
      error,
    });
  }
};
const updateTrialStatusSaga = function* ({ payload }: any) {
  try {
    yield put({
      type: UPDATE_TRIAL_STATUS_REQUEST,
    });
    const res = yield call(() => {
        return axios.put(`${API}api/modules/clinical_trial/${payload.ClinicalTrialID}/patient/${payload.MemberID}/conditions/${payload.ConditionID}`, payload);
      },
    );

    yield loadTrialMemberDetailsSaga({payload});

  } catch (error) {
    yield put({
      type: UPDATE_TRIAL_STATUS_ERROR,
      error,
    });
  }
};


export const saga = function* () {
  yield all([
    takeEvery(LOAD_ALL, loadAllSaga),
    takeEvery(LOAD_TRIAL_MEMEBER_DETAILS, loadTrialMemberDetailsSaga),
    takeEvery(LOAD_TRIAL_MEMEBER, loadTrialMembersSaga),
    takeEvery(UPDATE_TRIAL_STATUS, updateTrialStatusSaga),
  ]);
};
