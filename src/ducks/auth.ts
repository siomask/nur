import { API, appName } from '../config';
import { Record } from 'immutable';
import { all, cps, call, put, take, takeEvery } from 'redux-saga/effects';
import axios from 'axios';
import { createSelector } from 'reselect';


const access_token = localStorage.getItem('access_token');
if (access_token) applyHeader(access_token);

function applyHeader(access_token: string = '') {
  axios.defaults.headers.common['authorization'] = access_token;
  if (access_token) {
    localStorage.setItem('access_token', access_token);
  } else {
    localStorage.removeItem('access_token');
    delete axios.defaults.headers.common['authorization'];
  }
}

export const ReducerRecord = Record({
  user: null,
  userMedicationStatementInfo: null,
  userFamilyInfo: null,
  userAllergiesInfo: null,
  userLabResultInfo: null,
  userVitalInfo: null,
  loading: false,
  isChecked: false,
  error: null,
});

export const moduleName = 'auth';
export const SIGN_OUT = `${appName}/${moduleName}/SIGN_OUT`;
export const SIGN_OUT_SUCCESS = `${appName}/${moduleName}/SIGN_OUT_SUCCESS`;
export const SIGN_OUT_ERROR = `${appName}/${moduleName}/SIGN_OUT_ERROR`;
export const SIGN_IN = `${appName}/${moduleName}/SIGN_IN`;
export const SIGN_IN_REQUEST = `${appName}/${moduleName}/SIGN_IN_REQUEST`;
export const SIGN_IN_ERROR = `${appName}/${moduleName}/SIGN_IN_ERROR`;
export const SIGN_IN_SUCCESS = `${appName}/${moduleName}/SIGN_IN_SUCCESS`;
export const USER_LOAD = `${appName}/${moduleName}/USER_LOAD`;
export const USER_LOAD_REQUEST = `${appName}/${moduleName}/USER_LOAD_REQUEST`;
export const USER_LOAD_SUCCESS = `${appName}/${moduleName}/USER_LOAD_SUCCESS`;
export const USER_LOAD_ERROR = `${appName}/${moduleName}/USER_LOAD_ERROR`;
export const USER_ABOUT = `${appName}/${moduleName}/USER_ABOUT`;
export const USER_ABOUT_REQUEST = `${appName}/${moduleName}/USER_ABOUT_REQUEST`;
export const USER_ABOUT_SUCCESS = `${appName}/${moduleName}/USER_ABOUT_SUCCESS`;
export const USER_ABOUT_ERROR = `${appName}/${moduleName}/USER_ABOUT_ERROR`;
export const SIGN_OUT_REQUEST = `${appName}/${moduleName}/SIGN_OUT_REQUEST`;
export const VITAL_SIGN_INFO = `${appName}/${moduleName}/VITAL_SIGN_INFO`;
export const VITAL_SIGN_INFO_REQUEST = `${appName}/${moduleName}/VITAL_SIGN_INFO_REQUEST`;
export const VITAL_SIGN_INFO_SUCCESS = `${appName}/${moduleName}/VITAL_SIGN_INFO_SUCCESS`;
export const VITAL_SIGN_INFO_ERROR = `${appName}/${moduleName}/VITAL_SIGN_INFO_ERROR`;


export default function reducer(state = new ReducerRecord(), action: any) {
  const { type, payload, error } = action;

  switch (type) {
    case USER_ABOUT_REQUEST:
    case SIGN_OUT_REQUEST:
    case SIGN_IN_REQUEST:
      return state.set('loading', true);

    case SIGN_IN_SUCCESS:
      return state
        .set('loading', false)
        .set('error', null);

    case VITAL_SIGN_INFO_SUCCESS: {
      return state
        .set('loading', false)
        .set('isChecked', true)
        .set('userVitalInfo', action.payload.data.userVitalInfo)
        .set('userLabResultInfo', action.payload.data.userLabResultInfo)
        .set('userMedicationStatementInfo', action.payload.data.MedicationStatement)
        .set('userAllergiesInfo', action.payload.data.AllergyIntolerance)
        .set('userFamilyInfo', action.payload.data.FamilyTree)
        .set('error', null);
    }
    case USER_ABOUT_SUCCESS: {
      return state
        .set('loading', false)
        .set('isChecked', true)
        .set('user', action.payload)
        .set('error', null);
    }
    case USER_ABOUT_ERROR: {
      return state
        .set('loading', false)
        .set('isChecked', true)
        .set('error', null);
    }

    case SIGN_OUT_ERROR:
    case SIGN_IN_ERROR:
      return state
        .set('loading', false)
        .set('isChecked', true)
        .set('error', error);

    case SIGN_OUT_SUCCESS: {
      return new ReducerRecord({
        isChecked: true,
      });
    }

    default:
      return state;
  }
}


export const stateSelector = (state: any) => state[moduleName];
export const userSelector = createSelector(stateSelector, state => state.user);
export const userVitalInfoSelector = createSelector(stateSelector, state => state.userVitalInfo);
export const userLabResultSelector = createSelector(stateSelector, state => state.userLabResultInfo);
export const userAllergiesSelector = createSelector(stateSelector, state => state.userAllergiesInfo);
export const userMedicationStatementSelector = createSelector(stateSelector, state => state.userMedicationStatementInfo);
export const userFamilyInfoSelector = createSelector(stateSelector, state => state.userFamilyInfo);


export function signOut() {
  return {
    type: SIGN_OUT,
  };
}

export function signIn(user: any) {
  return {
    type: SIGN_IN,
    payload: {
      UserName: user.email,
      Password: user.password,
    },
  };
}

export function loadUser() {
  return {
    type: USER_LOAD,
  };
}

export function getVitalSignsInfo() {
  return {
    type: VITAL_SIGN_INFO,
  };
}


const getVitalSignsInfoSaga = function* () {
  try {
    yield put({
      type: VITAL_SIGN_INFO_REQUEST,
    });
    const res = yield call(() => {
        return axios.get(`${API}api/user/observation`);
      },
    );

    yield put({
      type: VITAL_SIGN_INFO_SUCCESS,
      payload: res,
    });

  } catch (error) {
    yield put({
      type: VITAL_SIGN_INFO_ERROR,
      error,
    });
  }
};
const signOutSaga = function* () {
  try {
    yield put({
      type: SIGN_OUT_REQUEST,
    });
    const res = yield call(() => {
        return axios.get(`${API}api/user/logout`);
      },
    );
    applyHeader();
    yield put({
      type: SIGN_OUT_SUCCESS,
    });

  } catch (error) {
    yield put({
      type: SIGN_OUT_ERROR,
      error,
    });
  }
};
const signInSaga = function* (action: any) {
  try {
    yield put({
      type: SIGN_IN_REQUEST,
    });
    const res = yield call(() => {
        return axios.post(`${API}auth/login`, action.payload);
      },
    );
    applyHeader(res.data.access_token);
    yield put({
      type: SIGN_IN_SUCCESS,
    });
    yield loadInfo();

  } catch (error) {
    yield put({
      type: SIGN_IN_ERROR,
      error,
    });
  }
};

const loadUserSaga = function* () {
  try {
    yield loadInfo();
  } catch (error) {
    yield put({
      type: USER_LOAD_ERROR,
      error,
    });
  }
};


const loadInfo = function* () {
  try {
    yield put({
      type: USER_ABOUT_REQUEST,
    });
    const res = yield call(() => {
        return axios.get(`${API}api/user/about`);
      },
    );
    const user = {
      info: res.data,
    };
    yield put({
      type: USER_ABOUT_SUCCESS,
      payload: user,
    });


  } catch (error) {
    yield put({
      type: USER_ABOUT_ERROR,
      error,
    });
  }
};


export const saga = function* () {
  yield all([
    takeEvery(SIGN_IN, signInSaga),
    takeEvery(USER_LOAD, loadUserSaga),
    takeEvery(SIGN_OUT, signOutSaga),
    takeEvery(VITAL_SIGN_INFO, getVitalSignsInfoSaga),
  ]);
};
