import { API, appName } from '../config';
import { Record } from 'immutable';
import { all, cps, call, put, take, takeEvery } from 'redux-saga/effects';
import axios from 'axios';
import { createSelector } from 'reselect';
import { arrayToMap } from '../utils';
import { SIGN_OUT_SUCCESS } from './auth';


const _Record: any = Record;
const TextAnalysisRecord: any = new _Record({
  DocumentType: null,
  TextCreated: null,
  SourceRowID: null,
  PatientID: null,
  Created: null,
  Text: null,
  Dictionaries: null,
  ModuleConfiguration: null,
  TextStructure: null,
});


export const ReducerRecord = Record({
  loading: false,
  error: null,

  details: [],
  comments: [],
  patient: null,
  items: arrayToMap([], TextAnalysisRecord),
});

export const moduleName = 'text_analysis';
export const LOAD_ALL = `${appName}/${moduleName}/LOAD_ALL`;
export const LOAD_ALL_REQUEST = `${appName}/${moduleName}/LOAD_ALL_REQUEST`;
export const LOAD_ALL_SUCCESS = `${appName}/${moduleName}/LOAD_ALL_SUCCESS`;
export const LOAD_ALL_ERROR = `${appName}/${moduleName}/LOAD_ALL_ERROR`;
export const LOAD_PATIENT_REQUEST = `${appName}/${moduleName}/LOAD_PATIENT_REQUEST`;
export const LOAD_PATIENT_SUCCESS  = `${appName}/${moduleName}/LOAD_PATIENT_SUCCESS`;
export const LOAD_PATIENT_ERROR  = `${appName}/${moduleName}/LOAD_PATIENT_ERROR`;
export const LOAD_PATIENT = `${appName}/${moduleName}/LOAD_PATIENT`;


export default function reducer(state = new ReducerRecord(), action: any) {
  const { type, payload, error } = action;

  switch (type) {

    case SIGN_OUT_SUCCESS:{
      return new ReducerRecord();
    }

    case LOAD_PATIENT_REQUEST:
    case LOAD_ALL_REQUEST:
      return state.set('loading', true);

    case LOAD_ALL_SUCCESS:
      return state
        .set('loading', false)
        .set('items', arrayToMap(action.payload.items.map((el: any, index: any) => {
          el.id = index;
          return el;
        }), TextAnalysisRecord, 'id'))
        .set('details', action.payload.details||[])
        .set('comments', action.payload.comments||[])
        .set('error', null);

      case LOAD_PATIENT_SUCCESS:
      return state
        .set('loading', false)
        .set('patient', payload)
        .set('error', null);

    case LOAD_PATIENT_ERROR:
    case LOAD_ALL_ERROR: {
      return state
        .set('loading', false)
        .set('error', error);
    }

    default:
      return state;
  }
}


export const stateSelector = (state: any) => state[moduleName];
export const textAnalysisSelector = createSelector(stateSelector, state => state.items);
export const textAnalysisComments = createSelector(stateSelector, state => state.comments);
export const textAnalysisDetails = createSelector(stateSelector, state => state.details);

export const textAnalysis = createSelector(textAnalysisSelector, (items) => {
  return items.valueSeq().toArray();
});
export const patient = createSelector(stateSelector, state => state.patient);


export function loadAll(patientId: any,ClinicalTrialID:any) {
  return {
    type: LOAD_ALL,
    payload: {
      ClinicalTrialID,
      patientId,
    },
  };
}
export function loadPatient(patientId: any) {
  return {
    type: LOAD_PATIENT,
    payload: {
      patientId,
    },
  };
}


const loadAllSaga = function* ({ payload: { patientId ,ClinicalTrialID} }: any) {
  try {
    yield put({
      type: LOAD_ALL_REQUEST,
    });
    const res = yield call(() => {
        return axios.get(`${API}api/patient/${patientId}/text-analysis/${ClinicalTrialID}`);
      },
    );
    yield put({
      type: LOAD_ALL_SUCCESS,
      payload: res.data,
    });

  } catch (error) {
    yield put({
      type: LOAD_ALL_ERROR,
      error,
    });
  }
};
const loadPatientSaga = function* ({ payload: { patientId } }: any) {
  try {
    yield put({
      type: LOAD_PATIENT_REQUEST,
    });
    const res = yield call(() => {
        return axios.get(`${API}api/patient/${patientId}`);
      },
    );
    yield put({
      type: LOAD_PATIENT_SUCCESS,
      payload: res.data.data,
    });

  } catch (error) {
    yield put({
      type: LOAD_PATIENT_ERROR,
      error,
    });
  }
};

export const saga = function* () {
  yield all([
    takeEvery(LOAD_ALL, loadAllSaga),
    takeEvery(LOAD_PATIENT, loadPatientSaga),
  ]);
};
