import React, { Component } from 'react';
import { Link, NavLink } from 'react-router-dom';
import './index.scss';
import Logo from '../../../assets/img/logo.png';

import Arrow from '../../../assets/img/icons/down-arrow.svg';

import ReactSVG from 'react-svg';


class NavBar extends Component<{ menu: Array<any> }, {}> {

  render() {
    const { menu } = this.props;
    return (
      <aside className={'aside'}>
        <div className={'main-navbar'}>
          <div className={'logo'}>
            <Link to={'/'}> <img src={Logo}/></Link>
          </div>
        </div>
        <div className={'list-navbar'}>
          {
            menu.map((item) => {
              return (
                <NavLink to={item.route} key={item.title} activeClassName="active">
                  <div className={'menu-item d-flex j-btw'}>
                    <div className={'d-flex a-c'}>
                      {item.icon}
                      <span className={'menu-item-title'}>{item.title}</span>
                    </div>
                    {
                      !item.wisthouArrow ? (<ReactSVG svgClassName={'arrow'} src={Arrow}/>) : null
                    }

                  </div>
                </NavLink>
              );
            })
          }
        </div>
      </aside>
    );
  }
}


export default NavBar;
