import React, { Component } from 'react';

import ReactSVG from 'react-svg';
import './index.scss';
import Settings from '../../../assets/img/icons/settings.svg';
import { calculateAge } from '../../../ducks/utils';
import Avatar from '../../../assets/img/Elder Patient Image.jpg';
import HeaderMenu from './menu';
import HeaderSearch from './search.area';
import { connect } from 'react-redux';


class Header extends Component<{ user: any }, {}> {

  render() {
    return (
      <header className={'header d-flex f-col'}>

        <div className={'d-flex fullWidth a-c person-data'}>
          <div className={'d-flex a-c person-info'}>
            <div className="person-avata"><img src={Avatar}/></div>
            <div className={'d-flex a-c f-col text-left j-start person-desc'}>
              <span className="person-name">{this.props.user.info.name ? this.props.user.info.name[0].text : ''}</span>
              <span className="person-description">{calculateAge(this.props.user.info.birthDate)} Years Old,  <span
                style={{ textTransform: 'capitalize' }}>{this.props.user.info.gender}</span></span>
            </div>
          </div>

          <div className={'header-info fullWidth'}>

            <div className={'d-flex j-end a-c'}>
              <ReactSVG svgClassName={'settings'} src={Settings}/>
              <span className={'separator'}></span>
              <HeaderMenu/>
            </div>
          </div>

        </div>
        <HeaderSearch showRefresh/>
      </header>
    );
  }
}

const mapStateToProps = (state: any) => ({
  user: state.user.user,
});

export default connect(mapStateToProps)(Header);
