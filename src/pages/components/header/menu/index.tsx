import React, { Component } from 'react';

import ReactSVG from 'react-svg';
import './index.scss';
import Arrow from '../../../../assets/img/icons/down-arrow.svg';
import user from '../../../../assets/img/icons/user.svg';
import { bindActionCreators } from 'redux';
import { signOut } from '../../../../ducks/auth';
import { connect } from 'react-redux';


class HeaderMenu extends Component<{ signOut: any }, {}> {

  render() {
    return (
      <div className={'header-menu d-flex a-c'}>
        <ReactSVG svgClassName={'menu-avatar'} src={user}/>
        <ReactSVG svgClassName={'menu-arrow'} src={Arrow}/>

        <div className={'menu-dropdown'}>
          <div className={'menu-dropdown-item'} onClick={this.props.signOut}>Log Out</div>
        </div>
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch: any) => (
  bindActionCreators({
    signOut,
  }, dispatch)
);
export default connect(null, mapDispatchToProps)(HeaderMenu);

