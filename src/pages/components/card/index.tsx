import React, { Component } from 'react';
import './index.scss';

class Card extends Component<{ title: any },
  {}> {

  render() {
    return (
      <section className={'my-card'}>
        <div className={'card-title text-font-24'}>{this.props.title}</div>
        <div className={'card-body'}>{this.props.children}</div>
      </section>
    );
  }
}


export default Card;
