import React, { Component, Fragment } from 'react';
import './index.scss';
import HeaderSearch from '../header/search.area';
import { NavLink } from 'react-router-dom';

class ModuleHeader extends Component<{ title: string, breadcrumbs: any },
  {}> {

  render() {
    return null
    let breadcrumbs = this.props.breadcrumbs;
    breadcrumbs = breadcrumbs.map((el: any, index: number) => {
      return (
        <div key={el.name} className={'d-flex a-c'}>
          {
            index < breadcrumbs.length - 1 ? (
              <Fragment>
                <NavLink to={el.route} className={'link'}>
                <span
                  className={` bread-crumbs-previos breacrumb-item`}>{el.name}</span>
                </NavLink>
                <span className={'bread-crumbs-delimeter'}>\</span>
              </Fragment>
            ) : (
              <span
                className={`bread-crumbs-currentbreacrumb-item`}>{el.name}</span>
            )
          }
        </div>
      );
    });
    return (
      <Fragment>
        <div className={'col-8 '} style={{ padding: '10px 0' }}>
          <HeaderSearch showRefresh/>
        </div>
        <section className={'overview-header d-flex a-c j-btw'}>
          <div className={'bread-crumbs d-flex'}>
            {
              breadcrumbs
            }
          </div>
          <span className={'overview-title'}>{this.props.title}</span>
          <span className={'overview-point'}></span>
        </section>

      </Fragment>
    );
  }
}


export default ModuleHeader;
