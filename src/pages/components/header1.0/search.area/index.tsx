import React, { Component } from 'react';
import ReactSVG from 'react-svg';
import './index.scss';
import Search from '../../../../assets/img/icons/search.svg';
import reload from '../../../../assets/img/icons/reload.svg';
import download from '../../../../assets/img/icons/download.svg';
import { bindActionCreators } from 'redux';
import { getVitalSignsInfo, loadUser } from '../../../../ducks/auth';
import { connect } from 'react-redux';
import { loadTrialMembers, loadAll, loadTrialMemberDetails } from '../../../../ducks/clinical_trials';

import { withRouter, RouteComponentProps } from 'react-router';
import { Loading } from '../../../../components/loading';
import Icon from '@material-ui/core/Icon';


interface MatchParams {
  ClinicalTrialID: string;
  MemberID: string;
}

interface Props extends RouteComponentProps<MatchParams> {
  loadAllTrials: Function,
  loadTrialMemberDetails: Function,
  loadTrialMembers: Function,
  loadUser: Function,
  getVitalSignsInfo: Function,
  showPlaceHolder?: boolean,
  showRefresh: boolean
}

function highlight(text: string, content: string) {
  var innerHTML = content + '';
  var index = innerHTML.toLowerCase().indexOf(text.toLowerCase());
  if (index >= 0) {
    innerHTML = innerHTML.substring(0, index) + '<span class=\'highlight\'>' + innerHTML.substring(index, index + text.length) + '</span>' + innerHTML.substring(index + text.length);

  }
  return innerHTML;
}

class HeaderSearch extends Component<Props, { refreshing: boolean, search: string, origin: string }> {

  state = {
    origin: '',
    search: '',
    refreshing: false,
  };
  static defaultProps: {
    showPlaceHolder: false
  };

  private onnSearch = (e: any) => {
    const search = e.target.value;
    this.setState({
      search,
    });

    const text: any = document.body.querySelector('.part-container');
    if (text) {
      const textTags = ['SPAN', 'DIV', 'P'];
      text.querySelectorAll('*').forEach((el: any) => {
        if (textTags.indexOf(el.tagName) > -1 && (el.children.length === 0 || el.children[0].className === 'highlight')) {
          el.innerHTML = highlight(search, el.innerText);
        }
      });

      // if (this.state.origin) {
      //   this.state.origin = text.innerHTML;
      //   this.setState({
      //     origin: text.innerHTML,
      //   });
      // }
      // text.innerHTML = highlight(search, this.state.origin);
      // text.
    }
  };

  private async refresh() {
    this.setState({ refreshing: true });
    await this.props.loadUser();
    await this.props.getVitalSignsInfo();
    await this.props.loadAllTrials();
    if (this.props.match.params.ClinicalTrialID) {
      await this.props.loadTrialMembers(this.props.match.params.ClinicalTrialID);
      if (this.props.match.params.MemberID) await this.props.loadTrialMemberDetails(this.props.match.params.ClinicalTrialID, this.props.match.params.MemberID);
    }
    setTimeout(() => {
      this.setState({ refreshing: false });
    }, 1000);
  }

  render() {
    return (
      <div className={'d-flex   a-c header-search'}>
        <div className={'d-flex a-c search-area j-end  '}>
          <div className={'search-container d-flex a-c  '}>
            <div className={'search-input d-flex a-c'}>
              <ReactSVG src={Search} style={{ paddingLeft: 15  }}/>
              {
                !this.props.showPlaceHolder ?
                  <input value={this.state.search} onChange={this.onnSearch} placeholder={'Search'}/> :
                  <input value={this.state.search} onChange={this.onnSearch} placeholder={'Search'}/>
              }

            </div>
          </div>
          {
            this.props.showRefresh ? (
              <div className={'  d-flex a-c'} style={{ marginLeft: '20px' }}>
                {
                  this.state.refreshing ? (<Loading/>) :  <Icon onClick={() => this.refresh()}>refresh</Icon>
                }
              </div>
            ) : null
          }

        </div>
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch: any) => (
  bindActionCreators({
    loadUser,
    getVitalSignsInfo,
    loadTrialMemberDetails,
    loadTrialMembers,
    loadAllTrials: loadAll,
  }, dispatch)
);

const ShowTheLocationWithRouter = withRouter(HeaderSearch);
export default connect(null, mapDispatchToProps)(ShowTheLocationWithRouter);
