import React, { Component, Fragment } from 'react';

import ReactSVG from 'react-svg';
import './index.scss';
import Settings from '../../../assets/img/icons/settings.svg';
import { calculateAge } from '../../../ducks/utils';
import Avatar from '../../../assets/img/Elder Patient Image.jpg';
import HeaderMenu from './menu';
import HeaderSearch from './search.area';
import { connect } from 'react-redux';
import { userSelector } from '../../../ducks/auth';
import { NavLink, RouteComponentProps, withRouter } from 'react-router-dom';
import Icon from '@material-ui/core/Icon';


interface MatchParams {
  ClinicalTrialID: string;
  MemberID: string;
}

interface Props extends RouteComponentProps<MatchParams> {
  user: any
}

class Header extends Component<Props, {}> {

  render() {
    let path = '';
    let breadcrumbs: any = this.props.location.pathname
      .split('/')
      .filter((e: any) => e)
      .map((e: any) => ({
        name: e,
        route: path + '/' + e,
      }));
    breadcrumbs = breadcrumbs.map((el: any, index: number) => {
      return (
        <div key={el.name} className={'d-flex a-c'}>
          {
            index < breadcrumbs.length - 1 && breadcrumbs.length > 1 ? (
              <Fragment>
                <NavLink to={el.route} className={'link'}>
                <span
                  className={` bread-crumbs-previos breacrumb-item`}>{el.name}</span>
                </NavLink>
                <span className={'bread-crumbs-delimeter'}>
                  <Icon>keyboard_arrow_right</Icon>
                </span>
              </Fragment>
            ) : (
              <span
                className={`bread-crumbs-currentbreacrumb-item`}>{el.name}</span>
            )
          }
        </div>
      );
    });

    return (
      <header className={'header d-flex f-col col-12 a-c'}>

        <div className={'d-flex fullWidth a-c person-data fullHeight j-btw'}>
          <div className={'bread-crumbs d-flex'}>
            {
              breadcrumbs
            }
          </div>
          <div className={'header-info   d-flex a-c'}>
            <HeaderSearch showRefresh/>
            <div className={'d-flex j-end a-c'}>
              <Icon style={{marginRight:20}}>filter_list</Icon>
              <Icon style={{marginRight:20}}>settings</Icon>
              <HeaderMenu/>
            </div>
          </div>

        </div>

      </header>
    );
  }
}

const mapStateToProps = (state: any) => ({
  user: userSelector(state),
});

const T: any = (connect(mapStateToProps)(Header));
export default withRouter(T);
