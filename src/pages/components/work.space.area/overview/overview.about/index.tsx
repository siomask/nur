import React, { Component } from 'react';
import './index.scss';
import Card from '../../../card';
import Accounting from '../../../../../assets/img/icons/mars.svg';
import download from '../../../../../assets/img/icons/download.svg';
import Avatar from '../../../../../assets/img/Elder Patient Image.jpg';
import ReactSVG from 'react-svg';
import { calculateAge } from '../../../../../ducks/utils';
import { userSelector } from '../../../../../ducks/auth';
import { connect } from 'react-redux';
import ModuleHeader from '../../../module.header';

class OverviewAbout extends Component<{ user: any },
  {}> {

  getUserEmail = () => {
    const { telecom = [] } = this.props.user.info || {};
    return (telecom.find((item: any) => item.system === 'email') || {}).value;
  };

  getUserPhone = () => {
    const { telecom = [] } = this.props.user.info || {};
    return (telecom.find((item: any) => item.use === 'home' && item.system === 'phone') || {}).value;
  };

  render() {
    const { address = [] } = this.props.user.info || {};
    const breadCrumbs = [
      { name: 'About' },
    ];
    return (
      <section className={'overview-about col-12'}>
        <ModuleHeader
          title={'About'}
          breadcrumbs={breadCrumbs}
        />

        <div className={'row mt'}>
          <div className={'col-4'} style={{ padding: '0 10px' }}>
            <Card title={'About'}>

              <div className={'row a-c  card-item-row d-flex j-btw'}>
                <span className={'card-label'}>Name</span>
                <span
                  className={'card-label-info'}>{this.props.user.info.name ? this.props.user.info.name[0].text : ''}</span>
              </div>
              <div className={'row a-c  card-item-row d-flex j-btw'}>
                <span className={'card-label'}>Date of birth </span>
                <div className={'card-label-info d-flex a-c'}>
                  <span>{this.props.user.info.birthDate}</span>
                  <div className={'card-label-ad-info d-flex a-c f-col'}>
                    <span className={'years-old'}>{calculateAge(this.props.user.info.birthDate)}</span>
                    <span className={'old'}>Years Old</span>
                  </div>
                </div>
              </div>
              <div className={'row a-c  card-item-row d-flex j-btw'}>
                <span className={'card-label'}>Gender</span>
                <div className={'card-label-info d-flex a-c'}>
                  <span style={{ textTransform: 'capitalize' }}>{this.props.user.info.gender}</span>
                  <div className={'card-label-ad-info d-flex a-c f-col'}>
                    <ReactSVG src={Accounting}/>
                  </div>
                </div>
              </div>
              <div className={'row a-c  card-item-row d-flex j-btw'}>
                <span className={'card-label'}>Languages</span>
                <span className={'card-label-info'}>English, Spanish</span>
              </div>
            </Card>
            <br/>
            <Card title={'Emergency Contact'}>

              <div className={'row a-c  card-item-row d-flex j-btw'}>
                <span className={'card-label'}>Name</span>
                <span className={'card-label-info'}>Emergency Contact</span>
              </div>
              <div className={'row a-c  card-item-row d-flex j-btw'}>
                <span className={'card-label'}>Phone</span>
                <span className={'card-label-info'}>785-3562-8763</span>
              </div>
              <div className={'row a-c  card-item-row d-flex j-btw'}>
                <span className={'card-label'}>Mobile</span>
                <span className={'card-label-info'}>269-3562-8763</span>
              </div>

            </Card>
          </div>
          <div className={'col-4'} style={{ padding: '0 10px' }}>
            <Card title={'Contact Details'}>

              <div className={'row a-c  card-item-row d-flex j-btw'}>
                <span className={'card-label'}>Email</span>
                <span className={'card-label-info'}>
                  {this.getUserEmail()}
                </span>
              </div>
              <div className={'row a-c  card-item-row d-flex j-btw'}>
                <span className={'card-label'}>Phone</span>
                <span className={'card-label-info'}>
                {this.getUserPhone()}
                </span>
              </div>
              <div className={'row a-c  card-item-row d-flex j-btw'}>
                <span className={'card-label'}>Address </span>
                <span className={'card-label-info'}>
                  {(address[0] || {}).text}
                </span>
              </div>

            </Card>
          </div>
          <div className={'col-4'} style={{ padding: '0 10px' }}>
            <div className={'d-flex j-center f-col person-data'}>
              <img src={Avatar}/>
              <br/>
              <button className={'d-flex a-c my-btn my-primary '}>
                <ReactSVG src={download}/>
                <span>EXPORT DATA</span>
                <ReactSVG src={download}/>
              </button>
            </div>
          </div>
        </div>
      </section>
    );
  }
}


const mapStateToProps = (state: any) => ({
  user: userSelector(state),
});

export default connect(mapStateToProps)(OverviewAbout);
