import React, { Component } from 'react';
import { connect } from 'react-redux';
import Table from '../../../common/Table';
import { bindActionCreators } from 'redux';
import ReactSVG from 'react-svg';
import './index.scss';

import Arrow from '../../../../../assets/img/icons/left-arrow.svg';
import Scale from '../../../../../assets/img/icons/scale.svg';
import Height from '../../../../../assets/img/icons/height.svg';
import Cardio from '../../../../../assets/img/icons/cardio.svg';
import BloodPressure from '../../../../../assets/img/icons/blood-pressure.svg';
import BodyTemperature from '../../../../../assets/img/icons/allergies.svg';
import HeartRate from '../../../../../assets/img/icons/heart-rate.svg';
import Mould from '../../../../../assets/img/icons/mould.svg';
import Body from '../../../../../assets/img/icons/body.png';

import { getVitalSignsInfo, userVitalInfoSelector } from '../../../../../ducks/auth';
import ModuleHeader from '../../../module.header';

class VitalSigns extends Component<{
  user: any,
  getVitalSignsInfo: Function,
  userVitalInfo: any,
},
  {}> {
  items: { img: any, property: string; value: string; measureUnit: any; }[];

  constructor(props: any) {
    super(props);
    this.items = [
      /*{
        img: Scale,
        property: 'Weight',
        value: '',
        measureUnit: '',
      },
      {
        img: Height,
        property: 'Height',
        value: '',
        measureUnit: '',
      },
      {
        img: Cardio,
        property: 'BMI',
        value: '',
        measureUnit: '',
      },*/
      {
        img: BloodPressure,
        property: 'Blood Pressure',
        value: '',
        measureUnit: '',
      },
      {
        img: BodyTemperature,
        property: 'Body temperature',
        value: '',
        measureUnit: '',
      },
      {
        img: Mould,
        property: 'Respiratory Rate',
        value: '',
        measureUnit: '',
      },
      {
        img: HeartRate,
        property: 'Heart Rate',
        value: '',
        measureUnit: '',
      },
    ];
  }

  componentDidMount() {
    this.props.getVitalSignsInfo();
  }

  modifyDataForTable = () => {
    const { userVitalInfo } = this.props;
    return this.items.map((item: any, index) => {

      const userDataFromServer = userVitalInfo.entry ? userVitalInfo.entry.find(
        (itemFromServer: any) => item.property === itemFromServer.resource.code.text || itemFromServer.resource.code.text.toLowerCase().match(item.property.toLowerCase())) : null;
      return userDataFromServer ? {
        id: index,
        img: item.img,
        property: userDataFromServer.resource.code.text,
        value: userDataFromServer.resource.valueQuantity ?
          userDataFromServer.resource.valueQuantity.value :
          (Array.isArray(userDataFromServer.resource.component) ?
            userDataFromServer.resource.component
              .sort((a: any, b: any) => a.code.coding[0] && a.code.coding[0].display.toLowerCase().match('diastolic') ? -1 : 1)
              .map((el: any) => el.valueQuantity.value).join('/') : ''),
        measureUnit: userDataFromServer.resource.valueQuantity ?
          userDataFromServer.resource.valueQuantity.unit :
          (Array.isArray(userDataFromServer.resource.component) ? userDataFromServer.resource.component[0].valueQuantity.unit : '')
        ,
        arrow: true,
      } : {
        ...item,
        id: index,
      };
    });
  };

  getSvgForProperty = (property: string) => {
    return <ReactSVG svgClassName={'menu-icon'} src={property}/>;
  };

  healthLine = (percent: number) => {
    return {
      top: `${percent}px`,
    };
  };

  render() {
    const { userVitalInfo } = this.props;
    const columns = [
      {
        title: <p>Property</p>,
        dataIndex: 'property img',
        render: (property: any, img: any) => (
          <div className="cell-content property">
            <ReactSVG svgClassName={'vital-icon'} src={img}/>
            <p>{property}</p>
          </div>
        ),
      },
      {
        title: <p>Value</p>,
        dataIndex: 'value',
        render: (value: any) =>
          <div className="cell-content text-center value">
            <p>{value}</p>
          </div>,
      },
      {
        title: <p>Measure Unit</p>,
        class: 'w-15',
        dataIndex: 'measureUnit',
        render: (measureUnit: any) =>
          <div className="cell-content text-center unit">
            {measureUnit}
          </div>,
      },
      {
        title: <p/>,
        dataIndex: 'arrow',
        render: (arrow: boolean) => (
          <div className="cell-content text-center">
            {arrow && <ReactSVG svgClassName={'arrow'} src={Arrow}/>}
          </div>
        ),
      },
      {
        title: <p/>,
        dataIndex: '',
        render: () => (
          <div className="cell-content wrapper-button">
            <button className="button" type="button" onClick={() => console.log('more details')}>
              more details
            </button>
          </div>
        ),
      },
    ];
    return (
      <section className={'overview-about'}>
        <div className="vital-signs">
          <ModuleHeader
            title={'Vital Signs'}
            breadcrumbs={[{ name: 'Vital Signs' }]}
          />
          {userVitalInfo
            ? <div className="vital-signs-content">
              <div className="table-cvontainer">
                <Table
                  rowId={'id'}
                  dataSource={this.modifyDataForTable()}
                  columns={columns}
                />
              </div>
              {/*<div className="health">*/}
              {/*/!* <img src={Body} alt="health"/> *!/*/}
              {/*<div className="health-level" style={this.healthLine(40)}/>*/}
              {/*</div>*/}
            </div>
            : <p>Loading...</p>
          }
        </div>
      </section>
    );
  }
}

const mapStateToProps = (state: any) => ({
  userVitalInfo: userVitalInfoSelector(state),
});

const mapDispatchToProps = (dispatch: any) => (
  bindActionCreators({
    getVitalSignsInfo,
  }, dispatch)
);

export default connect(mapStateToProps, mapDispatchToProps)(VitalSigns);
