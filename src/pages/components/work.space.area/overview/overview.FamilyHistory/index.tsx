import React, { Component } from 'react';
import { connect } from 'react-redux';
import Table from '../../../common/Table';
import { bindActionCreators } from 'redux';
import ReactSVG from 'react-svg';
import './index.scss';


import { getVitalSignsInfo, userFamilyInfoSelector } from '../../../../../ducks/auth';
import ModuleHeader from '../../../module.header';
import FamilyDesc from './Family.desc';
import FamilyTree from './famile.tree';

class FamilyHistory extends Component<{
  user: any,
  getVitalSignsInfo: Function,
  userVitalInfo: any,
},
  {}> {
  items: { img: any, property: string; value: string; measureUnit: any; }[];

  constructor(props: any) {
    super(props);
    this.items = [];
  }

  componentDidMount() {
    this.props.getVitalSignsInfo();
  }


  getSvgForProperty = (property: string) => {
    return <ReactSVG svgClassName={'menu-icon'} src={property}/>;
  };

  healthLine = (percent: number) => {
    return {
      top: `${percent}px`,
    };
  };

  private parseData() {
    const node: any = {
      name: 'YOU',
      children: [],
    };
    let index = 1;
    if (!this.props.userVitalInfo) return null;
    const list = this.props.userVitalInfo.entry;
    if (!list) return node;
    for (let i = 0; i < list.length; i++) {
      const _item: any = list[i].resource;
      const item: any = _item.relationship.coding[0];
      const diseases = [];

      for (let j = 0; j < _item.condition.length; j++) {
        for (let k = 0; k < _item.condition[j].code.coding.length; k++) {
          diseases.push({
            ..._item.condition[j].code.coding[k],
            index: index++,
          });
        }
      }
      node.children.push({
        diseases,
        display: item.display,
        name: item.display,
        children: [],
      });
    }
    return node;
  }

  render() {
    const { userVitalInfo } = this.props;
    const row = this.parseData();
    return (
      <section className={'overview-about'}>
        <div className="vital-signs">
          <ModuleHeader
            title={'Family History'}
            breadcrumbs={[{ name: 'Family History' }]}
          />
          {userVitalInfo
            ? <div className="vital-signs-content">
              <div className={'row fullWidth'}>
                <div className={'col-9'}>
                  <div className="family-overview d-flex a-c j-center">

                    <FamilyTree node={row}/>
                  </div>
                </div>
                <div className={'col-3'}>
                  <FamilyDesc node={row}/>
                </div>
              </div>
            </div>
            : <p>Loading...</p>
          }
        </div>
      </section>
    );
  }
}

const mapStateToProps = (state: any) => ({
  userVitalInfo: userFamilyInfoSelector(state),
});

const mapDispatchToProps = (dispatch: any) => (
  bindActionCreators({
    getVitalSignsInfo,
  }, dispatch)
);

export default connect(mapStateToProps, mapDispatchToProps)(FamilyHistory);
