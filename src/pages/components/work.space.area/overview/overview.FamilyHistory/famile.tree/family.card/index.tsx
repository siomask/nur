import React, { Component } from 'react';
import './index.scss';


import father from '../../../../../../../assets/img/icons/father.svg';
import mother from '../../../../../../../assets/img/icons/mother.svg';
import man from '../../../../../../../assets/img/icons/man.svg';
import grandmother_red from '../../../../../../../assets/img/icons/grandmother_red.svg';
import grandmother_blue from '../../../../../../../assets/img/icons/grandmother_blue.svg';
import grandfather from '../../../../../../../assets/img/icons/grandfather.svg';
import ReactSVG from 'react-svg';

export default class FamilyCard extends Component<{
  node: any,
},
  {}> {


  render() {
    const { node } = this.props;
    let icon = man;
    switch (node.display) {
      case 'mother': {
        icon = mother;
        break;
      }
      case 'father': {
        icon = man;
        break;
      }
    }
    return (
      <div className={'card-view'}>

        <div className={'family-type'}>
          <ReactSVG svgClassName={'card-person-icon'} src={icon}/>
        </div>
        <div className={'family-name'}>{node.name}</div>
      </div>
    );
  }
}



