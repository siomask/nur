import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import './index.scss';
import FamilyCard from './family.card';


class FamilyTree extends Component<{
  node: any,
},
  {}> {

  private parserNode() {
    const { node } = this.props;
    const nodes: any = [[node]];

    (function lineArray(list, level) {

      const row=[];
      let _level = 0;
      while (_level++ < level && list[0] && list[0].children.length) {
        const temptList = [];
        for (let i = 0; i < list.length; i++) {
          for (let j = 0; j < list[i].children.length; j++) {
            temptList.push(list[i].children[j]);
          }

        }
        list = temptList;
      }
      nodes.push(list);
      if (!list[0] || !list[0].children.length) return;
      lineArray(list, level + 1);

    })(node.children, 0);
    return nodes;
  }


  private rows() {
    const { node } = this.props;
    const rows = [[node]];

    (function _node(node) {
      if (node.children.length) {
        rows.push(node.children);
        _node(node.children[0]);
      }
    })(node);
    return rows;
  }

  render() {
    const { node } = this.props;

    const rows:any = this.parserNode();
    return (
      <div className={'family-tree-node fullWidth d-flex f-col-rev'}>
        {
          rows.map((el:any, index:any) => (
            <div className={'d-flex f-row j-around fullWidth row-node-item'} key={`node-item-${index}`}>
              {
                el.map((_node:any, _ind:any) => (
                  <div
                    className={`node-item fullWidth ${index === 0 ? 'first-item' : ''} ${index === rows.length - 1 ? 'last-item' : ''}`}
                    key={`node-item-${index}-${_ind}`}>
                    <div className={'connection-line'}></div>
                    <div className={'card-item-el d-flex j-center'}>
                      <div className={'connection-point'}></div>
                      <FamilyCard node={_node}/>
                    </div>
                  </div>
                ))
              }
            </div>
          ))
        }

      </div>
    );
  }
}

const mapStateToProps = (state: any) => ({});

const mapDispatchToProps = (dispatch: any) => (
  bindActionCreators({}, dispatch)
);

export default connect(mapStateToProps, mapDispatchToProps)(FamilyTree);
