import React, { Component } from 'react';
import './index.scss';
import ReactSVG from 'react-svg';
import Avatar from '../../../../../../assets/img/Elder Patient Image.jpg';
import download from '../../../../../../assets/img/icons/download.svg';


export default class FamilyDesc extends Component<{
  node: any,
},
  {}> {


  private parserNode() {
    const { node } = this.props;
    const nodes: any = [];


    (function lineArray(list, level) {

      let _level = 0;
      while (_level++ < level && list[0] && list[0].children.length) {
        const temptList = [];
        for (let i = 0; i < list.length; i++) {
          for (let j = 0; j < list[i].children.length; j++) {
            temptList.push(list[i].children[j]);
          }

        }
        list = temptList;
      }
      for (let i = 0; i < list.length; i++) {
        nodes.push(list[i]);
      }
      if (!list[0] || !list[0].children.length) return;
      lineArray(list, level + 1);

    })(node.children, 0);
    return nodes;
  }

  render() {


    const nodes = this.parserNode();
    console.log(nodes);
    return (
      <div className={'row mt'}>

        <div className={'col-12'} style={{ padding: '0 10px' }}>
          <div className={'d-flex j-center f-col '}>

            <div className={'family-card-desc'}>
              {/*<div className={'d-flex fullWidth j-center'}>*/}
                {/*<img src={'http://cdn.flaticon.com/png/256/26051.png'}/>*/}
              {/*</div>*/}

              {
                nodes.map((el: any, index: any) => {
                  return (
                    <div key={index} className={'item-dis'}>
                      <div className={'level-title'}>{el.display}</div>
                      {
                        el.diseases.map((el: any) => (
                          <div className={'item-disease d-flex j-start'} key={el.index}>
                            <span className={'item-number'}>{el.index}.</span>
                            <span className={'item-disaply'}>{el.display}</span>
                          </div>
                        ))
                      }
                    </div>
                  );
                })
              }
            </div>
            <br/>
            <button className={'d-flex a-c my-btn my-primary '}>
              <ReactSVG src={download}/>
              <span>EXPORT DATA</span>
              <ReactSVG src={download}/>
            </button>
          </div>
        </div>
      </div>
    );
  }
}

