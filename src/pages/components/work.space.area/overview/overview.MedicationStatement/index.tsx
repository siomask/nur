import React, { Component } from 'react';
import { connect } from 'react-redux';
import Table from '../../../common/Table';
import { bindActionCreators } from 'redux';
import ReactSVG from 'react-svg';
import './index.scss';

import Warning from '../../../../../assets/img/icons/warning.svg';
import Arrow from '../../../../../assets/img/icons/left-arrow.svg';
import BloodPressure from '../../../../../assets/img/icons/blood-pressure.svg';
import BodyTemperature from '../../../../../assets/img/icons/allergies.svg';
import HeartRate from '../../../../../assets/img/icons/heart-rate.svg';
import Mould from '../../../../../assets/img/icons/mould.svg';

import { getVitalSignsInfo, userMedicationStatementSelector } from '../../../../../ducks/auth';
import ModuleHeader from '../../../module.header';

class MedicationStatement extends Component<{
  user: any,
  getVitalSignsInfo: Function,
  userVitalInfo: any,
},
  {}> {
  items: { img: any, property: string; value: string; measureUnit: any; }[];

  constructor(props: any) {
    super(props);
    this.items = [];
  }

  componentDidMount() {
    this.props.getVitalSignsInfo();
  }

  modifyDataForTable = () => {
    const { userVitalInfo } = this.props;
    if (!userVitalInfo.entry) return [];
    return userVitalInfo.entry.map((item: any, index: any) => {

      const userDataFromServer = item;
      return userDataFromServer ? {
        id: index,
        ...userDataFromServer.resource,
        Medication: userDataFromServer.resource.medicationCodeableConcept?userDataFromServer.resource.medicationCodeableConcept.coding.map((el:any)=>el.display).join(", "):'',
        Dosage: userDataFromServer.resource.dosage?userDataFromServer.resource.dosage.map((el:any)=>el.text).join(", "):'',
      } : {
        ...item,
        id: index,
      };
    });
  };

  getSvgForProperty = (property: string) => {
    return <ReactSVG svgClassName={'menu-icon'} src={property}/>;
  };

  healthLine = (percent: number) => {
    return {
      top: `${percent}px`,
    };
  };

  render() {
    const { userVitalInfo } = this.props;
    const columns = [
      {
        title: <p>Medication</p>,
        dataIndex: 'Medication',
        render: (property: any, row: any) => (
          <div className="cell-content property">
            <p>{property}</p>
          </div>
        ),
      },
      {
        title: <p className={'text-left'}>Dosage</p>,
        dataIndex: 'Dosage',
        render: (value: any, row: any) =>
          <div className="cell-content text-center  ">
            <p className={'text-left'}>{value}</p>
          </div>,
      },
    ];
    return (
      <section className={'overview-about'}>
        <div className="vital-signs">
          <ModuleHeader
            title={'Medications'}
            breadcrumbs={[{ name: 'Medications' }]}
          />
          {userVitalInfo
            ? <div className="vital-signs-content">
              <div className="table-cvontainer">
                <Table
                  rowId={'id'}
                  dataSource={this.modifyDataForTable()}
                  columns={columns}
                />
              </div>
            </div>
            : <p>Loading...</p>
          }
        </div>
      </section>
    );
  }
}

const mapStateToProps = (state: any) => ({
  userVitalInfo: userMedicationStatementSelector(state),
});

const mapDispatchToProps = (dispatch: any) => (
  bindActionCreators({
    getVitalSignsInfo,
  }, dispatch)
);

export default connect(mapStateToProps, mapDispatchToProps)(MedicationStatement);
