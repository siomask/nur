import React, { Component } from 'react';
import { connect } from 'react-redux';
import Table from '../../../common/Table';
import { bindActionCreators } from 'redux';
import ReactSVG from 'react-svg';
import './index.scss';

import Warning from '../../../../../assets/img/icons/warning.svg';
import Arrow from '../../../../../assets/img/icons/left-arrow.svg';
import BloodPressure from '../../../../../assets/img/icons/blood-pressure.svg';
import BodyTemperature from '../../../../../assets/img/icons/allergies.svg';
import HeartRate from '../../../../../assets/img/icons/heart-rate.svg';
import Mould from '../../../../../assets/img/icons/mould.svg';

import { getVitalSignsInfo, userAllergiesSelector } from '../../../../../ducks/auth';
import ModuleHeader from '../../../module.header';

class Allergies extends Component<{
  user: any,
  getVitalSignsInfo: Function,
  userVitalInfo: any,
},
  {}> {
  items: { img: any, property: string; value: string; measureUnit: any; }[];

  constructor(props: any) {
    super(props);
    this.items = [

      {
        img: BloodPressure,
        property: 'Blood Pressure',
        value: '',
        measureUnit: '',
      },
      {
        img: BodyTemperature,
        property: 'Body temperature',
        value: '',
        measureUnit: '',
      },
      {
        img: Mould,
        property: 'Respiratory Rate',
        value: '',
        measureUnit: '',
      },
      {
        img: HeartRate,
        property: 'Heart Rate',
        value: '',
        measureUnit: '',
      },
    ];
  }

  componentDidMount() {
    this.props.getVitalSignsInfo();
  }

  modifyDataForTable = () => {
    const { userVitalInfo } = this.props;
    if (!userVitalInfo.entry) return [];
    return userVitalInfo.entry.map((item: any, index: any) => {

      const userDataFromServer = item;
      let _Reaction = '';
      if (userDataFromServer.resource.reaction) {
        for (let i = 0; i < userDataFromServer.resource.reaction.length; i++) {
          const react = userDataFromServer.resource.reaction[i];
          for (let j = 0; j < react.manifestation.length; j++) {
            _Reaction += (_Reaction ? ', ' : '') + react.manifestation[j].coding.map((el: any) => el.display).join(', ');
          }
        }
      }
      const list = userDataFromServer.resource.code.coding.map((el: any) => el.display);
      const _Allergy = list.join(',');
      return userDataFromServer ? {
        id: index,
        ...userDataFromServer.resource,
        _Allergy,
        _Reaction,
      } : {
        ...item,
        id: index,
      };
    });
  };

  getSvgForProperty = (property: string) => {
    return <ReactSVG svgClassName={'menu-icon'} src={property}/>;
  };

  healthLine = (percent: number) => {
    return {
      top: `${percent}px`,
    };
  };

  render() {
    const { userVitalInfo } = this.props;
    const columns = [
      {
        title: <p>Allergy</p>,
        dataIndex: '_Allergy',
        render: (property: any, row: any) => (
          <div className="cell-content property">
            <p>{property}</p>
          </div>
        ),
      },
      {
        title: <p className={'text-left'}>Reaction</p>,
        dataIndex: '_Reaction',
        render: (value: any, row: any) =>
          <div className="cell-content text-center  ">
            <p className={'text-left'}>{value}</p>
          </div>,
      },
    ];
    return (
      <section className={'overview-about'}>
        <div className="vital-signs">
          <ModuleHeader
            title={'Allergies'}
            breadcrumbs={[{ name: 'Allergies' }]}
          />
          {userVitalInfo
            ? <div className="vital-signs-content">
              <div className="table-cvontainer">
                <Table
                  rowId={'id'}
                  dataSource={this.modifyDataForTable()}
                  columns={columns}
                />
              </div>
            </div>
            : <p>Loading...</p>
          }
        </div>
      </section>
    );
  }
}

const mapStateToProps = (state: any) => ({
  userVitalInfo: userAllergiesSelector(state),
});

const mapDispatchToProps = (dispatch: any) => (
  bindActionCreators({
    getVitalSignsInfo,
  }, dispatch)
);

export default connect(mapStateToProps, mapDispatchToProps)(Allergies);
