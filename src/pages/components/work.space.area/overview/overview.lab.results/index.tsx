import React, { Component } from 'react';
import { connect } from 'react-redux';
import Table from '../../../common/Table';
import { bindActionCreators } from 'redux';
import ReactSVG from 'react-svg';
import './index.scss';

import Warning from '../../../../../assets/img/icons/warning.svg';
import Arrow from '../../../../../assets/img/icons/left-arrow.svg';
import BloodPressure from '../../../../../assets/img/icons/blood-pressure.svg';
import BodyTemperature from '../../../../../assets/img/icons/allergies.svg';
import HeartRate from '../../../../../assets/img/icons/heart-rate.svg';
import Mould from '../../../../../assets/img/icons/mould.svg';

import { getVitalSignsInfo, userLabResultSelector } from '../../../../../ducks/auth';
import ModuleHeader from '../../../module.header';

class LabResults extends Component<{
  user: any,
  getVitalSignsInfo: Function,
  userVitalInfo: any,
},
  {}> {
  items: { img: any, property: string; value: string; measureUnit: any; }[];

  constructor(props: any) {
    super(props);
    this.items = [

      {
        img: BloodPressure,
        property: 'Blood Pressure',
        value: '',
        measureUnit: '',
      },
      {
        img: BodyTemperature,
        property: 'Body temperature',
        value: '',
        measureUnit: '',
      },
      {
        img: Mould,
        property: 'Respiratory Rate',
        value: '',
        measureUnit: '',
      },
      {
        img: HeartRate,
        property: 'Heart Rate',
        value: '',
        measureUnit: '',
      },
    ];
  }

  componentDidMount() {
    this.props.getVitalSignsInfo();
  }

  modifyDataForTable = () => {
    const { userVitalInfo } = this.props;
    if(!userVitalInfo.entry)return [];
    return userVitalInfo.entry.map((item: any, index: any) => {

      const userDataFromServer = item;
      return userDataFromServer ? {
        id: index,
        // img: item.img,
        property: userDataFromServer.resource.code.text || userDataFromServer.resource.code.coding[0].display,
        value: userDataFromServer.resource.valueQuantity ?
          userDataFromServer.resource.valueQuantity.value :
          (Array.isArray(userDataFromServer.resource.component) ?
            userDataFromServer.resource.component
              .sort((a: any, b: any) => a.code.coding[0] && a.code.coding[0].display.toLowerCase().match('diastolic') ? -1 : 1)
              .map((el: any) => el.valueQuantity.value).join('/') : ''),
        measureUnit: userDataFromServer.resource.valueQuantity ?
          userDataFromServer.resource.valueQuantity.unit :
          (Array.isArray(userDataFromServer.resource.component) ? userDataFromServer.resource.component[0].valueQuantity.unit : '')
        ,
        arrow: true,
        showWarning: typeof (userDataFromServer.resource.interpretation) !== 'number',
        ...userDataFromServer.resource,
      } : {
        ...item,
        id: index,
      };
    });
  };

  getSvgForProperty = (property: string) => {
    return <ReactSVG svgClassName={'menu-icon'} src={property}/>;
  };

  healthLine = (percent: number) => {
    return {
      top: `${percent}px`,
    };
  };

  render() {
    const { userVitalInfo } = this.props;
    const columns = [
      {
        title: <p>Lab Results</p>,
        dataIndex: 'property img',
        render: (property: any, img: any) => (
          <div className="cell-content property">
            <p>{property}</p>
          </div>
        ),
      },
      {
        title: <p>Value</p>,
        dataIndex: 'value',
        render: (value: any) =>
          <div className="cell-content text-center value">
            <p>{value}</p>
          </div>,
      },
      {
        title: <p>Measure Unit</p>,
        class: '',
        dataIndex: 'measureUnit',
        render: (measureUnit: any) =>
          <div className="cell-content text-center unit">
            {measureUnit}
          </div>,
      },
      {
        title: <p>Interpretation</p>,
        dataIndex: 'showWarning',
        render: (showWarning: boolean, row: any) => {
          const min = row.referenceRange[0].low.value;
          const max = row.referenceRange[0].high.value;
          return (
            <div className="cell-content text-center d-flex a-c">
              {
                (row.value > max || row.value < min) && (
                  <div title={`${row.value < min ? 'too low' : 'too hight'}`}><ReactSVG src={Warning}/></div>
                )
              }
              {
                row.referenceRange ? (
                  <div className={'interpretation fullWidth'}>


                    <div
                      className={`value ${row.value > max ? 'out right' : ''}  ${row.value < min ? 'out left' : ''}`}>
                      <span
                        className={`property-val `}>{row.value}</span>
                      <span className={'point'}></span>
                    </div>
                    <div className={'line'}></div>
                    <div className={'d-flex a-c j-a'}>
                      <span>{min}</span>
                      <span>{max}</span>
                    </div>
                  </div>
                ) : null
              }

            </div>
          );
        },
      },
    ];
    return (
      <section className={'overview-about'}>
        <div className="vital-signs">
          <ModuleHeader
            title={'Lab Results'}
            breadcrumbs={[{ name: 'Lab Results' }]}
          />
          {userVitalInfo
            ? <div className="vital-signs-content">
              <div className="table-cvontainer">
                <Table
                  rowId={'id'}
                  dataSource={this.modifyDataForTable()}
                  columns={columns}
                />
              </div>
            </div>
            : <p>Loading...</p>
          }
        </div>
      </section>
    );
  }
}

const mapStateToProps = (state: any) => ({
  userVitalInfo: userLabResultSelector(state),
});

const mapDispatchToProps = (dispatch: any) => (
  bindActionCreators({
    getVitalSignsInfo,
  }, dispatch)
);

export default connect(mapStateToProps, mapDispatchToProps)(LabResults);
