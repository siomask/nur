import React, { Component } from 'react';
import { Route, Switch } from 'react-router';
import {
  Redirect,
} from 'react-router-dom';

import './index.scss';
import VitalSigns from './overview/overview.vital.signs';
import LabResults from './overview/overview.lab.results';
import Allergies from './overview/overview.AllergyIntolerance';
import MedicationStatement from './overview/overview.MedicationStatement';
import OverviewAbout from './overview/overview.about';
import FamilyHistory from './overview/overview.FamilyHistory';
import HeaderMenu from '../header/menu';
import ReactSVG from 'react-svg';
import Settings from '../../../assets/img/icons/settings.svg';
import ClinicalTrialAbout from './clinical_trials/clinical.trial.about';
import ClinicalTrialWatchLists from './clinical_trials/clinical.trial.watchlists';
import Incidents from './EPIDEMIOLOGY/Incidents';
import { userSelector } from '../../../ducks/auth';
import { connect } from 'react-redux';
import Header from '../../components/header1.0';

interface Props {
  user: any
}

class WorkSpaceArea extends Component<Props, {}> {

  render() {
    const appid = this.props.user.info.app.map((el: any) => el.ApplicationID);
    const routes = [];
    if (appid.indexOf(1) > -1) {
      routes.push(
        {
          path: '/clinical-trials/:ClinicalTrialID',
          component: ClinicalTrialWatchLists,
        },
        {
          path: '/clinical-trials',
          component: ClinicalTrialAbout,
        },
      );
    }
    if (appid.indexOf(2) > -1) {
      routes.push(
        {
          path: '/about',
          component: OverviewAbout,
        },
        {
          path: '/vital-signs',
          component: VitalSigns,
        },
        {
          path: '/family-history',
          component: FamilyHistory,
        },
        {
          path: '/lab-results',
          component: LabResults,
        },
        {
          path: '/allergies',
          component: Allergies,
        },
        {
          path: '/medications',
          component: MedicationStatement,
        },
      );
    }
    if (appid.indexOf(3) > -1) {
      routes.push(
        {
          path: '/All-Incidents',
          component: Incidents,
        },
        {
          path: '/Recently-Opened',
          component: Incidents,
        },
        {
          path: '/Recently-Closed',
          component: Incidents,
        },
      );
    }
    return (
      <div className={'work-space-area'}>
        <Header/>
        <div className={'col-12'}>
          <Switch>
            {
              routes.map((el: any) => (<Route key={el.path} path={el.path} component={el.component}/>))
            }
            <Route path="/not-found" component={() => null}/>
            <Route exact path="*" render={() => (
              <Redirect to="/not-found"/>
            )}/>
          </Switch>
        </div>

      </div>
    );
  }
}

const mapStateToProps = (state: any) => ({
  user: userSelector(state),
});

export default connect(mapStateToProps)(WorkSpaceArea);
