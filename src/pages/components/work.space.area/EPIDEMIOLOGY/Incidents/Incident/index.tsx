import React, { Component } from 'react';
import './index.scss';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import moment from 'moment';
import { Icon } from '@material-ui/core';
import IconButton from '@material-ui/core/IconButton';

interface Props {
  el: any,
  selectCard: Function
  selectedCard: any
}

interface State {
  star: boolean
}

export default class Incident extends Component<Props, State> {

  state = {
    star: false,
  };


  render() {
    const { el, selectCard, selectedCard }: any = this.props;
    let commentsCount;
    try {
      commentsCount = el.Comments ? JSON.parse(el.Comments).length : '-';
    } catch (e) {

    }
    let statusClass: string = '';
    let statusText: string = '';
    switch (el.Status) {
      case'Closed':
      case'Close': {
        statusClass = 'close';
        break;
      }
      case'Open': {
        statusClass = 'open';
        break;
      }
    }
    const statusVal: any = (
      <span className={`${statusClass} el-status`}>{el.Status}</span>
    );
    return (
      <Card key={el.EIID}
            className={` incidents ${selectedCard && selectedCard.EIID === el.EIID ? 'selected' : ''}`}
            onClick={() => selectCard(el)}>
        <CardContent>
          <div className={'d-flex    j-btw'}>
            <table>
              <tbody>
              <tr>
                <td colSpan={2} className={'text-overflow'}>
                  <div className={'d-flex a-c'}>
                    <IconButton aria-label="like" size="small"
                                onClick={() => this.setState({ star: !this.state.star })}>
                      <Icon>{
                        this.state.star ? 'star' : 'star_border'
                      }</Icon>
                    </IconButton>

                    <span className={'text-orange-light text-font-16 text-height-22'}
                          style={{ paddingLeft: 5 }}>{el.EIID}</span>
                  </div>
                </td>
                <td className={'text-overflow'}>
                  {statusVal}
                </td>
              </tr>
              <tr>
                <td className={'text-overflow'} colSpan={3}>
                  <span className={'incident-name'}>{el.PatientName}</span>
                </td>
              </tr>
              <tr>
                <td className={'text-overflow'} colSpan={3}>
                                <span
                                  className={'incident-open'}
                                  title={moment(el.InsertDate).format('MMM Do, YYYY hh:mm A')}>Opened: &nbsp;{moment(el.InsertDate).format('MMM Do, YYYY hh:mm A')}</span>
                </td>
                {/*<td className={'text-overflow'} style={{ width: 170 }}>*/}
                  {/*/!*<span className={'incident-open'} title={el.OpenedBy}>{el.OpenedBy}</span>*!/*/}
                {/*</td>*/}
              </tr>
              <tr>
                <td className={'text-overflow'}>
                  <span className={'incident-reg'}>Case Num:</span>
                </td>
                <td className={'text-overflow snd-col'}>
                  <span className={'incident-reg'}>{el.CaseNum}</span>
                </td>
                <td className={'text-overflow'}>
                  <span className={'incident-reg'}>Bacteria:&nbsp;{el.BacteriaName}</span>
                </td>
              </tr>

              <tr>
                <td className={'text-overflow'}>
                  <span className={'incident-reg'}>Department:</span>
                </td>
                <td className={'text-overflow snd-col'}>
                  <span className={'incident-reg'}>{el.DepartmentAtAcquire}</span>
                </td>
              </tr>
              <tr>
                <td className={'text-overflow'}>
                  <span className={'incident-reg'}>Priority:</span>
                </td>
                <td className={'text-overflow snd-col'}>
                  <span className={'incident-reg'}>{el.Priority}</span>
                </td>
              </tr>


              </tbody>
            </table>
          </div>

        </CardContent>
      </Card>
    );
  }
}

