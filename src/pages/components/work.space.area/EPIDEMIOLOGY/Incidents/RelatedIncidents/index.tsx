import React, { Component } from 'react';
import './index.scss';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Button from '@material-ui/core/Button';
import { withRouter } from 'react-router';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';

interface Props {
  selectedItem: any
}

interface State {
  showModal: boolean,
}

class RelatedIncidents extends Component<Props, State> {

  state = {
    showModal: false,
  };

  private toggleViewModal = () => {
    this.setState({ showModal: !this.state.showModal });
  };

  render() {
    // const selectedItem = this.props.selectedItem;
    let history = 'None';
    return (
      <section className={'epidemiology col-12'}>
        <h3>Related({history})</h3>

        <div>Some Related Incidents</div>
      </section>
    );
  }
}


const mapStateToProps = (state: any) => ({});
const mapDispatchToProps = (dispatch: any) => (
  bindActionCreators({}, dispatch)
);
const T: any = connect(mapStateToProps, mapDispatchToProps)(RelatedIncidents);
export default withRouter(T);
