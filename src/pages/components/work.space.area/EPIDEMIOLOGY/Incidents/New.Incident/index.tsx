import React, { Component } from 'react';
import { incidentsBACTERIASelector, incidentsBACTERIA } from '../../../../../../ducks/EPIDEMIOLOGY/bacteria';
import { incidentsStatusSelector, incidentsStatus } from '../../../../../../ducks/EPIDEMIOLOGY/status';
import { addIncident } from '../../../../../../ducks/EPIDEMIOLOGY/incident';
import { _moduleName, loadAllIncidents } from '../../../../../../ducks/EPIDEMIOLOGY';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import Tooltip from '@material-ui/core/Tooltip';
import Button from '@material-ui/core/Button';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';

import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Slide from '@material-ui/core/Slide';
import { TransitionProps } from '@material-ui/core/transitions';
import FormControl from '@material-ui/core/FormControl';
import FormHelperText from '@material-ui/core/FormHelperText';

import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';

const Transition = React.forwardRef<unknown, TransitionProps>(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});
import { Route, Switch, withRouter } from 'react-router';
import { RouteComponentProps } from 'react-router';

type PathParamsType = {
  param1: string,
}
type Props = RouteComponentProps<PathParamsType> & {
  incidentsBACTERIA: Function,
  incidentsStatus: Function,
  loadAllIncidents: Function,
  addIncident: Function,
  incidentList: Number,
  status: Array<any>
  bacteria: Array<any>
}


interface State {
  open: boolean,
  error: any,
  form: any
}

const fieldsEls: any = [
  {
    label: 'Case Number',
    name: 'CaseNum',
    type: 'number',
  },
  {
    label: 'Patient ID',
    name: 'patientID',
  },
  {
    label: 'Patient Name',
    name: 'PatientName',
  },
  {
    label: 'Department at Acquire',
    name: 'DepartmentAtAcquire',
  },
  {
    label: 'Bacteria',
    name: 'BacteriaID',
    options: [],
  },
  {
    label: 'Status',
    name: 'EIStatusID',
    options: [],
  },
  {
    label: 'Priority',
    name: 'Priority',
    options: [1, 2, 3, 4, 5].map((el: any) => ({ id: el, value: el })),
  },
];

class NewIncidents extends Component<Props, State> {

  state = {
    open: false,
    error: {},
    form: {},
  };

  componentDidMount(): void {
    this.props.incidentsBACTERIA();
    this.props.incidentsStatus();
  }

  componentWillReceiveProps(nextProps: Readonly<Props>, nextContext: any): void {
    if (nextProps.incidentList !== this.props.incidentList) {
      this.props.loadAllIncidents();
    }

  }

  private handleClose = () => {
    this.setState({
      open: false,
      error: {},
      form: {},
    });
  };

  private showForm = () => {
    this.setState({ open: true });
  };

  private handleChange = (e: any) => {
    const s: any = {
      form: {
        ...this.state.form,
        [e.target.name]: e.target.type === 'number' ? parseFloat(e.target.value) : e.target.value,
      }, error: {},
    };
    this.setState(s);
  };
  private handleOk = (e: any) => {
    const form: any = this.state.form;
    for (let i = 0; i < fieldsEls.length; i++) {
      if (!form[fieldsEls[i].name]) {
        return this.setState({
          error: {
            [fieldsEls[i].name]: 'This field is required',
          },
        });
      }
    }
    this.props.addIncident(this.state.form);
    this.handleClose();
  };

  render() {
    const { open, error, form }: any = this.state;
    const fields = JSON.parse(JSON.stringify(fieldsEls));
    fields[4].options = this.props.bacteria.map((el: any) => ({ ...el, id: el.EIBacteriaID, value: el.BacteriaName }));
    fields[5].options = this.props.status.map((el: any) => ({ ...el, id: el.EIStatusID, value: el.Status }));
    return (
      <div>
        <Tooltip title="New Incident">
          <Fab color={'primary'} onClick={this.showForm}>
            <AddIcon/>
          </Fab>
        </Tooltip>
        <Dialog
          open={open}

          TransitionComponent={Transition}
          keepMounted
          onClose={this.handleClose}
          aria-labelledby="alert-dialog-slide-title"
          aria-describedby="alert-dialog-slide-description"
        >
          <DialogTitle id="alert-dialog-slide-title">{'New Incident'}</DialogTitle>
          <DialogContent>
            {
              open && (
                <DialogContentText id="alert-dialog-slide-description">
                  {
                    fields.map((el: any) => {
                      let input;
                      if (el.options) {
                        input = (
                          <Select
                            value={form[el.name]}
                            onChange={this.handleChange}
                            name={el.name}
                          >
                            {
                              el.options.map((el: any) => (<MenuItem key={el.id} value={el.id}>{el.value}</MenuItem>))
                            }
                          </Select>
                        );
                      } else {
                        input = (
                          <Input
                            type={el.type}
                            id="component-error"
                            value={form[el.name]}
                            name={el.name}
                            onChange={this.handleChange}
                            aria-describedby="component-error-text"
                          />
                        );
                      }
                      return (
                        <FormControl error={error[el.name]} key={el.name} className={'fullWidth'}>
                          <InputLabel htmlFor="component-error">{el.label}</InputLabel>
                          {input}
                          {
                            error[el.name] && (
                              <FormHelperText>{error[el.name]}</FormHelperText>
                            )
                          }
                        </FormControl>
                      );
                    })
                  }
                </DialogContentText>
              )
            }

          </DialogContent>

          <DialogActions>
            <Button onClick={this.handleClose} color="primary">
              Cancel
            </Button>
            <Button onClick={this.handleOk} color="primary">
              Ok
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}


const mapStateToProps = (state: any) => ({
  bacteria: incidentsBACTERIASelector(state),
  status: incidentsStatusSelector(state),
  incidentList: state[_moduleName].incidentList,
});
const mapDispatchToProps = (dispatch: any) => (
  bindActionCreators({
    loadAllIncidents,
    incidentsBACTERIA,
    addIncident,
    incidentsStatus,
  }, dispatch)
);
const T: any = connect(mapStateToProps, mapDispatchToProps)(NewIncidents);
export default withRouter(T);
