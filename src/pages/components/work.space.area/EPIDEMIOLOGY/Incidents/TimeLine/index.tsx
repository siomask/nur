import React, { Component } from 'react';
import './index.scss';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  incidentsTimeline,
  incidentsTimeLineSelector,
  _moduleName,
} from '../../../../../../ducks/EPIDEMIOLOGY/timeline';
import { Loading } from '../../../../../../components/loading';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import moment from 'moment';
import { DATE_FORMAT } from '../../../../../../config';
import ReactSVG from 'react-svg';
import PatientIcon from '../../../../../../assets/img/new_icons/patient.svg';
import FrontDeskIcon from '../../../../../../assets/img/new_icons/front-desk.svg';
import MRIIcon from '../../../../../../assets/img/new_icons/mri.svg';
import ThreeTestTubeIcon from '../../../../../../assets/img/new_icons/three-test-tubes.svg';
import TriangleIcon from '../../../../../../assets/img/new_icons/triangle.svg';
import XRayIcon from '../../../../../../assets/img/new_icons/x-ray.svg';

import Tooltip from '@material-ui/core/Tooltip';

interface Props {
  incidentsTimeline: Function
  list: Array<any>
  selectedItem: any
  loading: boolean
}

interface State {
}

class IncidentsTimeLine extends Component<Props, State> {

  state = {};

  componentDidMount(): void {
    this.props.incidentsTimeline(this.props.selectedItem);
  }

  private parseTimeLines = () => {
    const timeLisnes: any = {};

    const list = this.props.list.filter((el: any) => el.CaseNum === this.props.selectedItem.CaseNum);
    for (let i = 0; i < list.length; i++) {
      const item: any = list[i];
      const date = moment(item.EventDate).format('ddd  DD/MM');
      if (!timeLisnes[date]) {
        timeLisnes[date] = {
          events: [],
          title: date,
          titleFull: moment(item.EventDate).format('ddd, MMM Do, YYYY'),
        };
      }
      timeLisnes[date].events.push(item);
    }
    return Object.keys(timeLisnes).map((el: any) => ({
      ...timeLisnes[el],
      // title: moment(el).format(DATE_FORMAT),
    }));
  };

  private icon = (type: string): any => {
    switch (type) {
      case  'Hospital Admission': {
        return FrontDeskIcon;
      }
      case  'Transportation to Department"':
      case  'Department Admission': {
        return PatientIcon;
      }
      case  'Lab Results - Recieved':
      case  'Lab Results - Sent': {
        return ThreeTestTubeIcon;
      }
      case  'Alert': {
        return TriangleIcon;
      }
      case  'X-Ray': {
        return XRayIcon;
      }
      case  'CT':
      case  'MRI': {
        return MRIIcon;
      }
      default: {
        return MRIIcon;
      }
    }
  };

  render() {
    if (this.props.loading) {
      return (<Loading/>);
    }
    const list = this.parseTimeLines();
    if (list.length === 0) return (<div className={'fullWidth text-center'}>No Data!</div>);
    return (
      <section className={'d-flex col-12 timeline-list f-col'}>

        <div className={'d-flex timeline-cards'}>
          {
            list.map((el: any) => (
              <Card className={' card-item1'} key={el.title}>
                <CardContent className={'d-flex f-col'}>
                  <div className={'card-title'}>{el.title}</div>
                  <div className={'d-flex   flex-wrap '}>
                    {
                      el.events.map((event: any, index: number) => {
                        const icon = this.icon(event.EV_EventName);
                        return (
                          <Tooltip title={<span className={'font14'}>{event.EV_EventName}</span>} >
                            <ReactSVG svgClassName={`${event.EV_EventName === 'Alert' ? '' : 'timeline-icon'}`}
                                      src={icon} key={event.EV_EventName}/>
                          </Tooltip>

                        );
                      })
                    }
                  </div>

                </CardContent>
              </Card>
            ))
          }

        </div>
        <Card className={'card-item2'}>
          <CardContent className={'d-flex f-col'}>
            <div className={'d-flex f-col timeline-info'}>
              {
                list.map((el: any) => {
                  return (
                    <div key={el.title} className={'timeline-data'}>
                      <div className={'markers'}>
                        <span className={'main-marker'}><span></span></span>
                        <div className={'marker-items-list'}>
                          {
                            el.events.map((event: any, index: number) => (
                              <span className={'sub-marker'} key={event.EV_EventName + 'r'}></span>
                            ))
                          }
                        </div>
                      </div>
                      <div className={'timeline-record'}>
                        <p>{el.titleFull}</p>
                        <table>
                          <tbody>
                          {
                            el.events.map((event: any, index: number) => {
                              return (
                                <tr key={event.EV_EventName}>
                                  <td>{moment(event.EventDate).format('hh:mm')}</td>
                                  <td>{event.EV_EventName}</td>
                                </tr>
                              );
                            })
                          }
                          </tbody>
                        </table>
                      </div>
                    </div>
                  );
                })
              }
            </div>
          </CardContent>
        </Card>

      </section>
    );
  }
}


const mapStateToProps = (state: any) => ({
  list: incidentsTimeLineSelector(state),
  loading: state[_moduleName].loading,
});
const mapDispatchToProps = (dispatch: any) => (
  bindActionCreators({
    incidentsTimeline,
  }, dispatch)
);
export default connect(mapStateToProps, mapDispatchToProps)(IncidentsTimeLine);
