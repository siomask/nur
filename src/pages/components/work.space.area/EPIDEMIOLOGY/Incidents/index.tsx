import React, { Component } from 'react';
import './index.scss';
import { incidents, loadAllIncidents } from '../../../../../ducks/EPIDEMIOLOGY';
import { connect } from 'react-redux';
import ModuleHeader from '../../../module.header';
import { bindActionCreators } from 'redux';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import Tooltip from '@material-ui/core/Tooltip';

import { Route, Switch, withRouter } from 'react-router';
import IncidentDetails from './Details';
import { Icon } from '@material-ui/core';
import Incident from './Incident';
import NewIncidents from './New.Incident';
import { RouteComponentProps } from 'react-router';

type PathParamsType = {
  param1: string,
}
type Props = RouteComponentProps<PathParamsType> & {
  loadAllIncidents: Function,
  incidents: Array<any>
}


interface State {
  EIID: any,
  selectedCard: any
}

class Incidents extends Component<Props, State> {

  state = {

    EIID: null,
    selectedCard: null,
  };

  componentDidMount(): void {
    this.props.loadAllIncidents();
    let EIID: any = this.props.location.pathname.split('/')[3];
    if (EIID) {
      this.setState({ EIID: parseInt(EIID) });
    }
  }

  private selectCard = (selectedCard: any) => {
    this.setState({ selectedCard });
    this.props.history.push(`/All-Incidents/Incident/${selectedCard.EIID}/Timeline`);
  };
  private priorety = (priorety: any) => {
    switch (priorety) {
      case 1: {
        return 'Very High';
      }
      case 2: {
        return 'High';
      }
      case 3: {
        return 'Medium';
      }
      case 4: {
        return 'Low';
      }
      case 5: {
        return 'Very Low';
      }
      default: {
        return 'None';
      }
    }
  };


  render() {
    let selectedCard: any = this.state.selectedCard;
    let EIID: any = this.state.EIID;
    if (EIID && !selectedCard) selectedCard = this.props.incidents.filter((el: any) => el.EIID === EIID)[0];
    if (selectedCard) selectedCard = this.props.incidents.filter((el: any) => el.EIID === selectedCard.EIID)[0];
    const props: any = this.props;
    const breadCrumbs = [
      { name: props.match.path.substr(1) },
    ];
    return (
      <section className={'epidemiology col-12'}>
        <ModuleHeader
          title={props.match.path.substr(1)}
          breadcrumbs={breadCrumbs}
        />

        <div className={'row mt '}>
          <div style={{ maxHeight: 'calc(100vh - 72px)', width: 415, overflowX: 'hidden', overflowY: 'auto' }}>

            {
              this.props.incidents
                .map((el: any) => (
                    <Incident
                      key={el.EIID}
                      el={el}
                      selectedCard={selectedCard}
                      selectCard={this.selectCard}
                    />
                  ),
                )
            }
          </div>
          <div className={'col-8'}
               style={{ padding: 5, width: 'calc(100% - 415px)', maxHeight: 'calc(100vh - 72px)', overflow: 'auto' }}>


            {
              selectedCard && (
                <IncidentDetails
                  key={selectedCard.EIID}
                  selectedItem={selectedCard}
                />

              )
            }
            <NewIncidents/>

          </div>

        </div>
      </section>
    );
  }
}


const mapStateToProps = (state: any) => ({
  incidents: incidents(state),
});
const mapDispatchToProps = (dispatch: any) => (
  bindActionCreators({
    loadAllIncidents,
  }, dispatch)
);
const T: any = connect(mapStateToProps, mapDispatchToProps)(Incidents);
export default withRouter(T);
