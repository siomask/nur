import React, { Component } from 'react';
import './index.scss';
import { _moduleName, incidentsDETAILS, incidentsDETAILSSelector } from '../../../../../../ducks/EPIDEMIOLOGY/details';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Comments from '../Comments';
import History from '../History';
import RelatedIncidents from '../RelatedIncidents';
import IncidentsTimeLine from '../TimeLine';
import Actions from './Actions';
import LabResults from '../LabResults';
import Tasks from '../Tasks';
import { Loading } from '../../../../../../components/loading';
import './index.scss';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import IconButton from '@material-ui/core/IconButton';
import { Icon } from '@material-ui/core';
import { withRouter } from 'react-router';
import {RouteComponentProps} from "react-router";
type PathParamsType = {
  param1: string,
}
type Props = RouteComponentProps<PathParamsType> & {
  loading: boolean,
  selectedItem: any,
  incidentsDETAILS: Function,
  details: Array<any>
}

interface State {
  tab: number,
  star: boolean,
}

const TabsItems: any = [
  {
    label: 'Timeline',
  },
  {
    label: 'Comments',
  },
  {
    label: 'History',
  },
  {
    label: 'Related',
  },
  {
    label: 'Lab Results',
  },
  {
    label: 'Tasks',
  },
].map((el:any,index:number)=>({...el,index}));

class IncidentDetails extends Component<Props, State> {

  state = {
    tab: 0,
    star: false,
  };

  componentDidMount(): void {
    let tab: any = this.props.location.pathname.split('/')[4];
    if (tab) {
      this.setState({ tab:  TabsItems.filter((i:any,index:number)=>i.label===tab)[0].index });
    }
    this.props.incidentsDETAILS(this.props.selectedItem);
  }


  private handleTabChange = (e: any, tab: number) => {
    this.setState({ tab });
    const tabItem:any = TabsItems.filter((i:any,index:number)=>index===tab)[0];
    this.props.history.push(`/All-Incidents/Incident/${this.props.location.pathname.split("/")[3]}/${tabItem.label}`);
  };

  render() {

    let { tab }: any = this.state;
    const { loading } = this.props;
    let selectedCard: any = this.props.selectedItem;
    let details;
    try {
      details = JSON.parse(this.props.details[0].EventData);
    } catch (e) {
      details = {};
    }
    return (
      <div className={'d-flex f-col incident-details'}>
        <div className={'d-flex fullWidth    j-btw'}>
          <table className={'col-5'}>
            <tbody>
            <tr>
              <td rowSpan={2} style={{ width: '6%' }}>
                <IconButton aria-label="like" size="small"
                            onClick={() => this.setState({ star: !this.state.star })}>
                  <Icon>{
                    this.state.star ? 'star' : 'star_border'
                  }</Icon>
                </IconButton>

              </td>
              <td>
                <span className={'incident-main'}>{selectedCard.PatientID}</span>
              </td>
            </tr>
            <tr>
              <td colSpan={2}>
                <span className={'incident-main-1'}>{selectedCard.PatientName}</span>
              </td>
            </tr>
            </tbody>
          </table>
          <div>
            <Actions/>
          </div>
        </div>
        <br/>
        <div className={'d-flex fullWidth    j-btw flex-wrap'}>
          <table className={'col-6'}>
            <tbody>
            <tr>
              <td>Department:</td>
              <td>
                {
                  0 ? (<Loading/>) : <span>{details.Department || '-'}</span>
                }
              </td>
            </tr>
            <tr>
              <td>Room:</td>
              <td>
                {
                  0 ? (<Loading/>) : <span>{details.Room || '-'}</span>
                }
              </td>
            </tr>
            <tr>
              <td>Bed:</td>
              <td>
                {
                  0 ? (<Loading/>) : <span>{details.Bed || '-'}</span>
                }
              </td>
            </tr>
            </tbody>
          </table>
        </div>

        <div className={'tabs-list'}>
          <Tabs value={tab} onChange={this.handleTabChange} aria-label="simple tabs example">
            {
              TabsItems.map((el: any) => (
                <Tab label={el.label} key={el.label}/>
              ))
            }
          </Tabs>
        </div>
        {
          tab === 0 && (
            <div>
              <h3 className={'timeline-title'}>Course in Hospital Timeline</h3>
              <IncidentsTimeLine selectedItem={selectedCard}/>
            </div>
          )
        }
        {
          tab === 1 && (
            <Comments selectedItem={selectedCard}/>
          )
        }
        {
          tab === 2 && (
            <History selectedItem={selectedCard}/>
          )
        }
        {
          tab === 3 && (
            <RelatedIncidents/>
          )
        }
        {
          tab === 4 && (
            <LabResults selectedItem={selectedCard}/>
          )
        }
        {
          tab === 5 && (
            <Tasks selectedItem={selectedCard}/>
          )
        }
      </div>
    );
  }
}


const mapStateToProps = (state: any) => ({
  details: incidentsDETAILSSelector(state),
  loading: state[_moduleName].loading,
});
const mapDispatchToProps = (dispatch: any) => (
  bindActionCreators({
    incidentsDETAILS,
  }, dispatch)
);
export default connect(mapStateToProps, mapDispatchToProps)(withRouter(IncidentDetails));

