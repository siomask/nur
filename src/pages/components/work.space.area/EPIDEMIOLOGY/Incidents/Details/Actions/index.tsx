import React from 'react';
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import './index.scss';

export default function SimpleMenu() {
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);

  function handleClick(event: React.MouseEvent<HTMLButtonElement>) {
    setAnchorEl(event.currentTarget);
  }

  function handleClose() {
    setAnchorEl(null);
  }

  return (
    <div className={'incident-actions'}>
      <Button
        variant="contained"

        color="primary"
        onClick={handleClick}
      >
        Actions
      </Button>
      <Menu
        id="simple-menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
      >
        {
          [
            'Change Status',
            'Flag',
            'Add a reminder',
            'Add a new comment',
            'Resend alert',
            'New Incident',

          ].map((el: any) => (
            <MenuItem onClick={handleClose} key={el}>{el}</MenuItem>
          ))
        }
      </Menu>
    </div>
  );
}
