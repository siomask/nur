import React, { Component } from 'react';
import './index.scss';
import ReactSVG from 'react-svg';
import Arrow from '../../../../../../assets/img/icons/left-arrow.svg';
import Table from '../../../../common/Table';

interface Props {
  selectedItem: any
}

interface State {
}

export default class LabResults extends Component<Props, State> {

  state = {};

  render() {
    const selectedItem = this.props.selectedItem;
    let res;
    try {
      res = JSON.parse(selectedItem.LabResults);
      console.log(res);
      res = res.entry.map((el: any) => {
        if (!el.resource) return {};
        return {
          code: el.resource.code && el.resource.code.coding && el.resource.code.coding[0] ? el.resource.code.coding[0].display : '',
          interpretation: el.resource.interpretation && el.resource.interpretation[0] && el.resource.interpretation[0].coding && el.resource.interpretation[0].coding[0] ? el.resource.interpretation[0].coding[0].code : '',
          comparator: el.resource.valueQuantity ? el.resource.valueQuantity.comparator : '',
          value: el.resource.valueQuantity ? el.resource.valueQuantity.value : '',
        };
      });
    } catch (e) {
      console.log(e);
      return (<h3>No data!</h3>);
    }
    const columns = [
      {
        title: <p>Code</p>,
        dataIndex: 'code',
        render: (property: any) => (
          <div className="cell-content property  ">
            <p>{property}</p>
          </div>
        ),
      }, {
        title: <p>Value</p>,
        dataIndex: 'value',
        render: (property: any) => (
          <div className="cell-content    text-center">
            <p>{property}</p>
          </div>
        ),
      }, {
        title: <p>Comparator</p>,
        dataIndex: 'comparator',
        render: (property: any) => (
          <div className="cell-content    text-center">
            <p>{property}</p>
          </div>
        ),
      },
      {
        title: <p>Interpretation</p>,
        dataIndex: 'interpretation',
        render: (property: any) => (
          <div className="cell-content    text-center">
            <p>{property}</p>
          </div>
        ),
      },

    ];
    return (
      <section className={'epidemiology col-12'}>
        <h3>Lab Results</h3>

        <Table
          rowId={'code'}
          dataSource={res}
          columns={columns}
        />
      </section>
    );
  }
}


