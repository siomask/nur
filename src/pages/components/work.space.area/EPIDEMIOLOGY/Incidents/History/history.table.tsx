import React, { Component } from 'react';
import './index.scss';
import { editIncidentsComment } from '../../../../../../ducks/EPIDEMIOLOGY';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Paper from '@material-ui/core/Paper';
import { EditingState } from '@devexpress/dx-react-grid';
import {
  Grid,
  Table,
  TableHeaderRow,
  TableEditRow,
  TableEditColumn,
} from '@devexpress/dx-react-grid-material-ui';


interface Props {
  rows: any
}

interface State {
}

const getRowId = (row: any) => row.EIID;

export default class TableList extends Component<Props, State> {

  state = {};


  render() {
    const { rows }: any = this.props;
    if (rows.length === 0) return <h6 className={'fullWidth text-center'}>No Data!</h6>;

    const columns: any = [
      { name: 'EIID', columnName: 'EIID', title: 'Incident ID' ,getCellValue:(row:any)=>{
          return (<a href={`${location.origin}/All-Incidents/Incident/${row.EIID}`} target={'_blank'}>{row.EIID}</a> )
        }},
      { name: 'CaseNum', columnName: 'CaseNum', title: 'Case Num' },
      { name: 'PatientID', columnName: 'PatientID', title: 'PatientID' },
      { name: 'InsertDate', columnName: 'InsertDate', title: 'InsertDate' },
      { name: 'DepartmentAtAcquire', columnName: 'DepartmentAtAcquire', title: 'DepartmentAtAcquire' },
      { name: 'BacteriaName', columnName: 'BacteriaName', title: 'Bacteria' },
    ];
    // const columns = rows.length ? Object.keys(rows[0]).map((el: any) => ({ name: el , title: el==='EIID'?'Incident ID' :el })) : [];
    return (
      <Paper>
        <Grid
          rows={rows}
          columns={columns}
          getRowId={getRowId}
        >
          <Table/>
          <TableHeaderRow/>
        </Grid>
      </Paper>
    );
  }
}



