import React, { Component } from 'react';
import './index.scss';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { incidentsHISTORY, incidentsHISTORYSelector, _moduleName } from '../../../../../../ducks/EPIDEMIOLOGY/history';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import { Loading } from '../../../../../../components/loading';
import TableList from './history.table';

interface Props {
  selectedItem: any,
  incidentsHISTORY: Function,
  loading: boolean,
  list: Array<any>,

}

interface State {
  showModal: boolean,
}

class History extends Component<Props, State> {

  state = {
    showModal: false,
  };

  componentDidMount(): void {
    this.props.incidentsHISTORY(this.props.selectedItem);
  }

  private toggleViewModal = () => {
    this.setState({ showModal: !this.state.showModal });
  };

  render() {


    let rows = this.props.list
    // .filter((el: any) => el.CaseNum === this.props.selectedItem.CaseNum && el.PatientID === this.props.selectedItem.PatientID)
      .map((el: any) => el.toObject());
    let history = rows.length ? 'Known' : 'New';
    return (
      <section className={'epidemiology col-12'}>
        <h3>History({history})</h3>
        <br/>
        {
          this.props.loading ? (<Loading/>) : (
            <TableList rows={rows}/>
          )
        }
      </section>
    );
  }
}


const mapStateToProps = (state: any) => ({
  list: incidentsHISTORYSelector(state),
  loading: state[_moduleName].loading,
});
const mapDispatchToProps = (dispatch: any) => (
  bindActionCreators({
    incidentsHISTORY,
  }, dispatch)
);
export default connect(mapStateToProps, mapDispatchToProps)(History);
