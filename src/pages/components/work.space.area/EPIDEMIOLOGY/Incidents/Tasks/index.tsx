import React, { Component } from 'react';
import './index.scss';
import Table from '../../../../common/Table';
import moment from 'moment';
import { DATE_FORMAT1 } from '../../../../../../config';

interface Props {
  selectedItem: any
}

interface State {
}

export default class Tasks extends Component<Props, State> {

  state = {};

  render() {
    const selectedItem = this.props.selectedItem;
    let res;
    try {
      res = JSON.parse(selectedItem.Tasks);
      res = res.Tasks;
    } catch (e) {
      console.log(e, selectedItem.Tasks);
      return (<h3>No data!</h3>);
    }
    const columns = [
      {
        title: <p>Task</p>,
        dataIndex: 'Task',
        class: 'col-th',
        render: (property: any) => (
          <div className="cell-content    property text-center">
            <p>{property}</p>
          </div>
        ),
      },
      {
        title: <p>TaskID</p>,
        dataIndex: 'TaskID',
        class: 'col-th',
        render: (property: any) => (
          <div className="cell-content property  ">
            <p>{property}</p>
          </div>
        ),
      }, {
        title: <p>Status</p>,
        dataIndex: 'Status',
        class: 'col-th',
        render: (property: any) => (
          <div className="cell-content   property text-center">
            <p>{property}</p>
          </div>
        ),
      },
      {
        title: <p>LastUpdate</p>,
        dataIndex: 'LastUpdate',
        class: 'col-th',
        render: (property: any) => (
          <div className="cell-content   property text-center">
            <p>{moment(property).format(DATE_FORMAT1)}</p>
          </div>
        ),
      },
      {
        title: <p>Created</p>,
        dataIndex: 'Created',
        class: 'col-th',
        render: (property: any) => (
          <div className="cell-content   property  text-center">
            <p>{moment(property).format(DATE_FORMAT1)}</p>
          </div>
        ),
      },

    ];
    console.log(res);
    return (
      <section className={'epidemiology tasks col-12'}>
        <h3>Tasks</h3>

        <Table
          rowId={'TaskID'}
          dataSource={res}
          columns={columns}
        />
      </section>
    );
  }
}


