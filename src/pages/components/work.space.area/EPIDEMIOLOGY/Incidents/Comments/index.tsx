import React, { Component } from 'react';
import './index.scss';
import { incidents, loadAllIncidents } from '../../../../../../ducks/EPIDEMIOLOGY';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import CommentList from './comments.table';

interface Props {
  selectedItem: any
}

interface State {
  showComments: any,
}

class Comments extends Component<Props, State> {

  state = {
    showComments: false,
  };

  componentDidMount(): void {
  }

  private toggleViewComments = () => {
    this.setState({ showComments: !this.state.showComments });
  };

  render() {
    const selectedItem = this.props.selectedItem;
    let countComments = 0;
    let comments = [];
    try {
      comments = JSON.parse(selectedItem.Comments);
      countComments = comments.length;
    } catch (e) {


    }
    return (
      <section className={'epidemiology col-12'}>
        <h3>Comments({countComments})</h3>

        <CommentList selectedItem={selectedItem}/>
      </section>
    );
  }
}


const mapStateToProps = (state: any) => ({});
const mapDispatchToProps = (dispatch: any) => (
  bindActionCreators({}, dispatch)
);
export default connect(mapStateToProps, mapDispatchToProps)(Comments);
