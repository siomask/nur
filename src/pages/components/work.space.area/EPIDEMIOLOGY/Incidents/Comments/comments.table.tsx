import React, { Component } from 'react';
import './index.scss';
import { editIncidentsComment } from '../../../../../../ducks/EPIDEMIOLOGY';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Paper from '@material-ui/core/Paper';
import { EditingState } from '@devexpress/dx-react-grid';
import {
  Grid,
  Table,
  TableHeaderRow, TableColumnResizing,
  TableEditRow,
  TableEditColumn,
} from '@devexpress/dx-react-grid-material-ui';


interface Props {
  editIncidentsComment: Function
  selectedItem: any
}

interface State {
  editingStateColumnExtensions: Array<any>,
  showComments: any,
  rows: Array<any>,
}

const getRowId = (row: any) => row.id;

class CommentsList extends Component<Props, State> {

  state = {
    editingStateColumnExtensions: [
      {
        columnName: 'Date', editingEnabled: false,
      },
    ],
    showComments: false,
    rows: [],
  };

  componentDidMount(): void {
    const selectedItem = this.props.selectedItem;
    let comments = [];
    try {
      comments = JSON.parse(selectedItem.Comments);
      this.setRows(comments);
    } catch (e) {


    }
  }

  private commitChanges = ({ added, changed, deleted }: any) => {
    let changedRows;
    const rows: any = this.state.rows;
    if (added) {
      const startingAddedId = rows.length > 0 ? rows[rows.length - 1].id + 1 : 0;
      changedRows = [

        ...added.map((row: any, index: any) => ({
          id: startingAddedId + index,
          ...row,
          Date: new Date().toISOString(),
        })),
        ...rows,
      ];
    }
    if (changed) {
      changedRows = rows.map((row: any) => (changed[row.id] ? { ...row, ...changed[row.id] } : row));
    }
    if (deleted) {
      const deletedSet = new Set(deleted);
      changedRows = rows.filter((row: any) => !deletedSet.has(row.id));
    }


    this.props.editIncidentsComment({
      ...this.props.selectedItem.toObject(),
      Comments: changedRows.map((el: any) => ({ Comment: el.Comment, Date: el.Date })),
    });
    this.setRows(changedRows);
  };
  private setRows = (rows: any) => {
    this.setState({ rows: rows.map((el: any, index: number) => ({ ...el, editing:false,id: index })) });
  };

  private onRowClick = (props: any, e: any) => {
    let el = e.target.parentNode;
    props.row.editing = true;
    while (el.tagName !== 'TR') {
      el = el.parentNode;
    }
    el.querySelector('#edit').click();
  };

  render() {
    const { rows, editingStateColumnExtensions }: any = this.state;
    const opt: any = {
      columnExtensions: editingStateColumnExtensions,
    };
    const columns: any = [
      { name: 'Comment', columnName: 'Comment', title: 'Comment', width: window.innerWidth - 1020 },
      { name: 'Date', columnName: 'Date', title: 'Date', width: 190 },
    ].map((el: any) => {
      el.getCellValue = (row: any, col: any) => {
        if (row[el.name]&& row[el.name].length > 30 && !row.editing) {
          return (
            <span style={{ height: 30 }} title={row[el.name]}>{row[el.name]}</span>
          );
        } else {
          return row[el.name];
        }

      };
      return el;
    });
    return (
      <Paper>
        <Grid
          rows={rows}
          columns={columns}
          getRowId={getRowId}
        >
          <EditingState
            onCommitChanges={this.commitChanges}
            {...opt}
          />
          <Table
            cellComponent={(props) => (<Table.Cell
              className={props.row.__selected ? 'selected' : ''}
              {...props}
              onDoubleClick={(e: any) => this.onRowClick(props, e)}
            />)}
          />
          <TableColumnResizing defaultColumnWidths={columns}/>
          <TableHeaderRow/>
          <TableEditRow/>
          <TableEditColumn
            showAddCommand
            showEditCommand
            showDeleteCommand
          />
        </Grid>
      </Paper>
    );
  }
}


const mapStateToProps = (state: any) => ({});
const mapDispatchToProps = (dispatch: any) => (
  bindActionCreators({
    editIncidentsComment,
  }, dispatch)
);
export default connect(mapStateToProps, mapDispatchToProps)(CommentsList);


