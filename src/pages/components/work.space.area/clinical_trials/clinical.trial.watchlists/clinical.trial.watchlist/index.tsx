import React, { Component, Fragment } from 'react';
import './index.scss';
import { connect } from 'react-redux';
import {DATE_FORMAT} from '../../../../../../config';
import ModuleHeader from '../../../../module.header';
import Card from '../../../../card';
import Tabs, { Tab } from '../../../../tabs';
import { loadTrialMembers, trails, trial } from '../../../../../../ducks/clinical_trials';

import Table from '../../../../common/Table';

import { bindActionCreators } from 'redux';
import moment from 'moment';
import { NavLink, RouteComponentProps, withRouter } from 'react-router-dom';


interface MatchParams {
  ClinicalTrialID: string;
  MemberID: string;
}

interface Props extends RouteComponentProps<MatchParams> {
  loadTrialMemberDetails: Function,
  loadTrialMembers: Function,
  items: Array<any>,
  trial: any
}


class ClinicalTrialWatchList extends Component<Props,
  {}> {

  private tabs: any = {};


  modifyDataForTable(WatchListID: number) {
    return this.props.trial.patients.filter((el: any) => el.WatchListID === WatchListID);
  }

  private showCriteriaDetails(memberId: any, e: any) {
    e.preventDefault();
    const tab = this.props.trial.tabs[this.tabs.state.activeTabIndex];
    this.props.history.push(`/clinical-trials/${this.props.match.params.ClinicalTrialID}/${tab ? tab.WLType : -1}/${memberId}/criteria-details`);
  }

  private columns() {
    return [
      {
        title: <p>Patient ID</p>,
        dataIndex: 'MemberID',
        render: (property: any, img: any) => {

          return (
            <div className="cell-content property text-center link"
                 onClick={(e: any) => this.showCriteriaDetails(property, e)}>
              <a className={'link'} href={'#'}>{property}</a>
            </div>
          );
        },
      },
      {
        title: <p className={'text-left'}>Patient Name</p>,
        dataIndex: 'MemberFullName',
        render: (property: any, img: any) => (
          <div className="cell-content property text-blue text-center">
            <p>{property}</p>
          </div>
        ),
      },
      {
        title: <p  className={'text-left'}>Added</p>,
        dataIndex: 'LastUpdate1',
        render: (property: any, item: any) => {
          const date = moment(item.LastUpdate).format(DATE_FORMAT);
          return (
            <div className="cell-content property text-blue text-center">
              <p>{date==='Invalid date'?'':date}</p>
            </div>
          );
        },
      },
      {
        title: <p  className={'text-left'}>Last update</p>,
        dataIndex: 'LastUpdate',
        render: (property: any, img: any) => {
          const date = moment(property).format(DATE_FORMAT);
          return (
            <div className="cell-content property text-blue text-center">
              <p>{date==='Invalid date'?'':date}</p>
            </div>
          );
        },
      },
      {
        title: <p  className={'text-left'}>Inclusion</p>,
        dataIndex: 'inclusion',
        render: (property: any, item: any) => {
          return (
            <div className="cell-content property text-blue text-center">
              <p className={'text-green'}>{item.INC ? item.INC.Count_Success : 0}</p>
              <p className={'text-red'}>{item.INC ? item.INC.Count_Failed : 0}</p>
              <p className={'text-orange'}>{item.INC ? item.INC.Count_Unknown : 0}</p>
            </div>
          );
        },
      },
      {
        title: <p  className={'text-left'}>Exclusion</p>,
        dataIndex: 'exclusion',
        render: (property: any, item: any) => (
          <div className="cell-content property text-blue text-center">
            <p className={'text-green'}>{item.EXC ? item.EXC.Count_Success : 0}</p>
            <p className={'text-red'}>{item.EXC ? item.EXC.Count_Failed : 0}</p>
            <p className={'text-orange'}>{item.EXC ? item.EXC.Count_Unknown : 0}</p>
          </div>
        ),
      },
    ];
  }

  render() {

    const tabs = this.props.trial.tabs.map((el: any, index: number) => {
      return {
        title: `${el.WLType} (${el.CountMembers})`,
        content: () => {
          return (
            <Table
              rowId={'MemberID'}
              dataSource={this.modifyDataForTable(el.WatchListID)}
              columns={this.columns()}
            />
          );
        },
      };
    });
    const id: number = parseInt(this.props.match.params.ClinicalTrialID);
    const item: any = this.props.items.filter((item) => item.ClinicalTrialID === id)[0];
    if (!item) return (<h1>No Clinical</h1>);
    const breadCrumbs = [
      { name: 'Clinical Trials', route: '/clinical-trials' },
      { name: item.Name, route: '/clinical-trials' },
      { name: 'Watch List' },
    ];
    return (
      <section className={'overview-about'}>
        <ModuleHeader
          title={'Watchlists'}
          breadcrumbs={breadCrumbs}
        />

        <div className={'row mt'}>

          <div className={'col-12'} style={{ padding: '0 10px' }}>
            <Card title={item.Name}>
              <Tabs
                defaultActiveTabIndex={0}
                ref={(el: any) => this.tabs = el}
                className={''}
              >
                {
                  tabs.map((tab: any) => (
                    <Tab title={tab.title} key={tab.title}>
                      {tab.content()}
                    </Tab>
                  ))
                }
              </Tabs>

            </Card>
          </div>
        </div>
      </section>
    );
  }
}


const mapStateToProps = (state: any) => ({
  items: trails(state),
  trial: trial(state),
});


const mapDispatchToProps = (dispatch: any) => (
  bindActionCreators({
    loadTrialMembers,
  }, dispatch)
);

const RouteClinicalTrialWatchList = withRouter(ClinicalTrialWatchList);

export default connect(mapStateToProps, mapDispatchToProps)(RouteClinicalTrialWatchList);
