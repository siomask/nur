import React, { Component, Fragment } from 'react';
import './index.scss';
import { connect } from 'react-redux';
import ModuleHeader from '../../../../module.header';
import SliderWLType from './sliderWlType';
import Slider from './slider';
import {
  loadTrialMembers,
  loadTrialMemberDetails,
  trails,
  clinicalMemberDetails,
  trial,
} from '../../../../../../ducks/clinical_trials';
import { bindActionCreators } from 'redux';
import TabsDetails from './tabs.details';

class ClinicalTrialWatchListCriteriaDetails extends Component<{
  loadTrialMembers: Function,
  loadTrialMemberDetails: Function,
  clinicalMemberDetails: Array<any>,
  items: Array<any>,
  trial: any,
  match: any
},
  {}> {


  async componentDidMount() {
    const { ClinicalTrialID, MemberID } = this.props.match.params;
    await this.props.loadTrialMembers(ClinicalTrialID);
    await this.props.loadTrialMemberDetails(ClinicalTrialID, MemberID);
  }


  render() {


    const id: number = parseInt(this.props.match.params.ClinicalTrialID);
    const item: any = this.props.items.filter((item) => item.ClinicalTrialID === id)[0];
    if (!item) return (<h1>No Clinical</h1>);
    const tab = this.props.trial.tabs.filter((el: any) => el.WLType === this.props.match.params.WLType)[0];
    if (!tab) return (<h1>No Tab</h1>);
    const patient = this.props.trial.patients.filter((el: any) => el.MemberID === this.props.match.params.MemberID)[0];
    // if (!patient) return (<h1>No Patient</h1>);

    const breadCrumbs = [
      { name: 'Clinical Trials', route: '/clinical-trials' },
      { name: item.Name, route: '/clinical-trials' },
      { name: 'Watch List', route: `/clinical-trials/${this.props.match.params.ClinicalTrialID}` },
      { name: tab.WLType, route: `/clinical-trials/${this.props.match.params.ClinicalTrialID}` },
      { name: this.props.match.params.MemberID, route: `/clinical-trials/${this.props.match.params.ClinicalTrialID}` },
      { name: 'Criteria details' },
    ];

    return (
      <section className={'overview-about'}>
        <ModuleHeader
          title={'Criteria Details'}
          breadcrumbs={breadCrumbs}
        />
        <div className={'row  '}>

          <div className={'d-flex  col-12 a-c '} style={{marginTop:10}}>
            <div className={' col-6 text-center '}>
              <SliderWLType/>
            </div>
            <div className={' col-6 '}>
              {
                patient && <Slider/>
              }

            </div>

          </div>


          <TabsDetails list={this.props.clinicalMemberDetails}/>
        </div>
      </section>
    );
  }
}


const mapStateToProps = (state: any) => ({
  items: trails(state),
  trial: trial(state),
  clinicalMemberDetails: clinicalMemberDetails(state),
});


const mapDispatchToProps = (dispatch: any) => (
  bindActionCreators({
    loadTrialMembers,
    loadTrialMemberDetails,
  }, dispatch)
);
export default connect(mapStateToProps, mapDispatchToProps)(ClinicalTrialWatchListCriteriaDetails);
