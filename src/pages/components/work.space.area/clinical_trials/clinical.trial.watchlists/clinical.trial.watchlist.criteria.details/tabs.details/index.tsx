import React, { Component } from 'react';
import './index.scss';
import Tabs, { Tab } from '../../../../../tabs';
import MemberDetails from './MemberDetails';


class TabsDetails extends Component<{ list: any },
  {}> {


  private content(index: string) {
    return this.props.list
      .filter((item: any) => item.Direction === index)
      .map((el: any, index: number) => (
        <MemberDetails item={el} key={index}/>
      ));
  }


  render() {
    const tabs = [
      {
        title: 'Inclusion',
        direction: 'INC',
      },
      {
        title: 'Exclusion',
        direction: 'EXC',
      },
    ];
    return (
      <div className={`col-12 member-tab-details `}  >
        <div className={'d-flex col-10'}>
          <Tabs defaultActiveTabIndex={0} className={'fullWidth '}>
            {
              tabs.map((tab: any) => {
                const data = this.props.list
                  .filter((item: any) => item.Direction === tab.direction).length ? this.content(tab.direction) : (
                  <h5>No Data</h5>);
                return (
                  <Tab title={tab.title} key={tab.title}>
                    <div>{data}</div>
                  </Tab>
                );
              })
            }
          </Tabs>
        </div>
      </div>
    );
  }
}

export default (TabsDetails);
