import React, { Component } from 'react';
import './index.scss';

import Popup from 'reactjs-popup';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { updateTrialStatus } from '../../../../../../../../../ducks/clinical_trials';
import ReactSVG from 'react-svg';
import comment from '../../../../../../../../../assets/img/icons/comment.svg';


class ChangeStates extends Component<{ item: any, MemberID: any, lastComment: any, updateTrialStatus: any }, { body: any, error: any }> {

  constructor(p: any) {
    super(p);
    this.state = {
      body: {
        Status: p.item.Status,
        Comment: p.item.Comment,
      },
      error: {},

    };
  }


  save = (next: Function) => {
    if (!this.state.body.Comment) {
      return this.setState({
        ...this.state,
        error: {
          ...this.state.error,
          Comment: 'Comment field is required',
        },
      });
    }
    const { ClinicalTrialID, ConditionID, id } = this.props.item;
    this.props.updateTrialStatus({
      ClinicalTrialID,
      MemberID: this.props.MemberID,
      ConditionID,
      id,
      ...this.state.body,
    });
    next();
    this.handleChange({ target: { name: 'Comment', value: '' } });
  };
  handleChange = (changeEvent: any) => {
    const newState: any = {
      body: {
        ...this.state.body,
        [changeEvent.target.name]: changeEvent.target.name === 'Status' ? parseInt(changeEvent.target.value) : changeEvent.target.value,
      },
      error: {
        ...this.state.error,
        [changeEvent.target.name]: !changeEvent.target.value,
      },

    };
    this.setState(newState);
  };
  addComent = (next: Function) => {
    next();
  };

  render() {
    const { item } = this.props;
    const { Status, Comment } = this.state.body;
    let statusClass;
    switch (item.Status) {
      case 0:
      case 'False': {
        statusClass = 'back-red';
        break;
      }
      case 2:
      case 'Unknown ': {
        statusClass = 'back-brown';
        break;
      }
      default: {
        statusClass = 'back-green';
      }
    }
    let modal, modalComment;
    if (item.UpdateType === 2) {
      const button = (<button className={'my-btn def-btn p-4 m-0'}>Change</button>);
      modal = (
        <Popup trigger={button} modal>
          {(close: any) => (
            <div className="modal status-popup">
              <a className="close" onClick={close}>
                &times;
              </a>
              <div className="header"> Change condition status</div>
              <div className="content">
                <form className={'my-form'}>
                  <div className={'d-flex'}>
                    <div className={'form-item-label col-2'}>New Status</div>
                    <div className={'form-item-value d-flex f-col'}>
                      <div className={'d-flex '}>
                        <input type="radio" id={'status0'} value="1" name="Status"
                               checked={Status === 1}
                               onChange={this.handleChange}
                        />
                        <label htmlFor="status0">Pass</label>
                      </div>
                      <div className={'d-flex '}>
                        <input type="radio" id={'status1'} value="0" name="Status"
                               checked={Status === 0}
                               onChange={this.handleChange}
                        />
                        <label htmlFor="status1">Failed</label>
                      </div>
                      <div className={'d-flex '}>
                        <input type="radio" id={'status2'} value="2" name="Status"
                               checked={Status === 2}
                               onChange={this.handleChange}
                        />
                        <label htmlFor="status2">UnKnown</label>
                      </div>
                    </div>
                  </div>
                  <div className={'d-flex f-col'}>
                    <div className={'form-item-label col-12'}>Reason for changing the status</div>
                    <div className={'form-item-value col-12'}>
                    <textarea rows={10} name={'Comment'} className={'col-12'} onChange={this.handleChange}>
                      {Comment}
                    </textarea>
                      {
                        this.state.error.Comment && <span className={'text-red'}>{this.state.error.Comment}</span>
                      }

                    </div>
                  </div>
                </form>

              </div>
              <div className="actions col-8">
                <button
                  className={`d-flex a-c my-btn pop-up-btn `}
                  onClick={() => {
                    this.save(close);
                  }}
                >
                  Save
                </button>
                <button
                  className={`d-flex a-c my-btn pop-up-btn `}
                  onClick={() => {
                    close();
                  }}
                >
                  Cancel
                </button>
              </div>
            </div>
          )}
        </Popup>
      );
    }

    if (this.props.lastComment) {
      const button = (<ReactSVG src={comment} className={'comment-svg'}/>);
      modalComment = (
        <Popup trigger={button} modal>
          {(close: any) => (
            <div className="modal status-popup">
              <a className="close" onClick={close}>
                &times;
              </a>
              <div className="header"> Add commnet</div>
              <div className="content">
                <form className={'my-form'}>

                  <div className={'d-flex f-col'}>
                    <div className={'form-item-label col-12'}>Your commnet</div>
                    <div className={'form-item-value col-12'}>
                    <textarea rows={10} name={'Comment'} className={'col-12'} onChange={this.handleChange}>
                      {Comment}
                    </textarea>

                    </div>
                  </div>
                </form>

              </div>
              <div className="actions col-8">
                <button
                  className={`d-flex a-c my-btn pop-up-btn `}
                  onClick={() => {
                    this.addComent(close);
                  }}
                >
                  Save
                </button>
                <button
                  className={`d-flex a-c my-btn pop-up-btn `}
                  onClick={() => {
                    close();
                  }}
                >
                  Cancel
                </button>
              </div>
            </div>
          )}
        </Popup>
      );
    }
    const elemB = (
      <div>

        <div className={'d-flex col-12  a-c'} style={{ position: 'relative' }}>
          <div className={'d-flex a-c'}>
            <div className={`member-details-status ${statusClass} `}></div>


          </div>

          <div className={`member-details-title d-flex j-around `}>
            <span style={{ marginRight: 60 }}> {item.Display ? item.Display : item.OriginalText}</span>


          </div>
          {modalComment}
        </div>

        <div className={'d-flex  col-12'}>
          <div className={'d-flex  col-6 d-column a-c'}>
            <span>Value:</span>
            <div className={'d-flex a-c'}>
              {
                item.Value ? (
                  <span className={`text-blue `}>{item.Value}</span>
                ) : <span className={`text-orange `}>unknown</span>
              }
              {modal}
            </div>


          </div>

          <div className={'d-flex  col-6 d-column a-c'}>
            <span>Updated By:</span>
            <span>{item.UpdatedBy}</span>
          </div>
        </div>
      </div>

    );
    return elemB;
  }
}

const mapDispatchToProps = (dispatch: any) => (
  bindActionCreators({
    updateTrialStatus,
  }, dispatch)
);
export default connect(null, mapDispatchToProps)(ChangeStates);
