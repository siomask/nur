import React, { Component } from 'react';
import './index.scss';
import moment from 'moment';
import { Link, RouteComponentProps, withRouter } from 'react-router-dom';
import comment from '../../../../../../../../assets/img/icons/comment.svg';
import document from '../../../../../../../../assets/img/icons/document.svg';
import ReactSVG from 'react-svg';
import ChangeStates from './ChangeStatus';
import { DATE_FORMAT } from '../../../../../../../../config';

interface MatchParams {
  MemberID: string;
}

interface Props extends RouteComponentProps<MatchParams> {
  item: any
}

class MemberDetails extends Component<Props,
  {}> {


  render() {

    const { item } = this.props;
    let typeText;
    let showText;
    let showStatus = 'Unknown';
    const updated = moment(item.LastUpdate).format(DATE_FORMAT);
    switch (item.UpdateType) {
      case 2: {
        typeText = 'Manual';
        showText = true;
        break;
      }
      default: {
        typeText = 'Auto';
      }
    }

    switch (item.Status) {
      case 0: {
        showStatus = 'Failed';
        break;
      }
      case 1: {
        showStatus = 'Pass';
        break;
      }
    }
    let lastComment;
    if (item.Comments && item.Comments.length) {
      lastComment = item.Comments.sort((a: any, b: any) => moment(a.InsertDate).isAfter(moment(b.InsertDate)) ? -1 : 1)[0];
    }
    console.log(item.ConditionID, item);
    return (
      <div className={'d-flex   col-12 member-details f-col'}>


        <ChangeStates item={item} lastComment={lastComment} MemberID={this.props.match.params.MemberID}/>

        <div className={'d-flex  col-12'}>
          <div className={'d-flex  col-6 d-column'}>
            <span>Status:</span>
            <span className={`text-blue `}>{showStatus}</span>

          </div>

        </div>

        <div className={'d-flex  col-12'}>
          <div className={'d-flex a-c col-6 d-column'}>
            <span>Type:&nbsp;</span>
            <span className={'d-flex a-c'}>{typeText}&nbsp;
              {
                showText ?
                  <a target={'_blank'}
                     href={`/text-analysis/legend/${item.ClinicalTrialID}/${this.props.match.params.MemberID}/${parseInt(item.ConditionID)}`}>
                    <button className={'my-btn def-btn p-4 m-0'}>View Text</button>
                  </a> : null
              }
              </span>
          </div>
          <div className={'d-flex  a-c col-6 d-column'}>
            <span>Last Update:</span>
            <span>{updated}</span>
          </div>
        </div>
        {
          lastComment ? (
            <div className={'d-flex  col-12'}>
              <div className={'d-flex  col-6 d-column'}>
                <span>Last Comment:</span>
                <span className={'text-overflow'}>
              {
                moment(lastComment.InsertDate).format(DATE_FORMAT)
              },&nbsp;
                  {
                    lastComment.Text
                  }
            </span>
              </div>
            </div>
          ) : null
        }

      </div>
    );
  }
}

const RouteMemberDetails = withRouter(MemberDetails);
export default RouteMemberDetails;
