import React, { Component } from 'react';
import './index.scss';
import { RouteComponentProps, withRouter } from 'react-router';
import { bindActionCreators } from 'redux';
import {
  loadTrialMemberDetails,
  clinicalMemberDetails,
  trails,
  trial,
} from '../../../../../../../ducks/clinical_trials';
import { connect } from 'react-redux';
import NextIcon from '../../../../../icons/next';


interface MatchParams {
  ClinicalTrialID: string;
  WLType: string;
  MemberID: string;
}

interface Props extends RouteComponentProps<MatchParams> {
  loadTrialMemberDetails: Function,
  trial: any
}

class SliderWLType extends Component<Props,
  {}> {


  private move(dir: number) {
    let { ClinicalTrialID } = this.props.match.params;
    let WLType: any;
    let memberId = -1;
    const items = this._items();
    for (let i = 0; i < items.length; i++) {
      if (items[i].WLType === this.props.match.params.WLType) {
        const next = items[i + dir];
        WLType = next.WLType;
        const patients = this.props.trial.patients.filter((el: any) => el.MemberID && el.WLType === WLType);
        memberId = patients[0] ? patients[0].MemberID : -1;
        break;
      }
    }
    this.props.history.push(`/clinical-trials/${ClinicalTrialID}/${WLType}/${memberId}/criteria-details`);
    this.props.loadTrialMemberDetails(ClinicalTrialID, memberId);
  }

  private _items() {
    return this.props.trial.tabs;
  }

  render() {
    const prev = '<';
    const next = '>';
    let curentItem;
    let hasPrev = false;
    let hasNext = false;
    let index = 0;
    const items = this._items();
    for (let i = 0; i < items.length; i++) {
      hasNext = i < items.length - 1;
      index = i + 1;
      if (items[i].WLType === this.props.match.params.WLType) {
        curentItem = items[i];
        break;
      }
      hasPrev = true;
    }
    if (!curentItem) {
      return <h1>No Watchlist</h1>;
    }
    return (
      <div className={'d-flex j-start col-12 wl-type-details-slider'}>
        <div className={'slider-container  d-flex a-c'}>
          <span onClick={() => this.move(-1)} className={`${hasPrev ? '' : 'disabled'} control`}> <NextIcon isNext={false}/> </span>
          <span className={'name text-orange '} title={`${curentItem.WLType} (${curentItem.CountMembers})`}>{curentItem.WLType}</span>
          <span> {index}/{items.length}</span>
          <span onClick={() => this.move(1)} className={`${hasNext ? '' : 'disabled'} control`}>
            <NextIcon isNext={true}/>
          </span>
        </div>
      </div>
    );
  }
}

const RouteSlider = withRouter(SliderWLType);
const mapStateToProps = (state: any) => ({
  items: trails(state),
  trial: trial(state),
  clinicalMemberDetails: clinicalMemberDetails(state),
});

const mapDispatchToProps = (dispatch: any) => (
  bindActionCreators({
    loadTrialMemberDetails,
  }, dispatch)
);
export default connect(mapStateToProps, mapDispatchToProps)(RouteSlider);
