import React, { Component } from 'react';
import './index.scss';
import { RouteComponentProps, withRouter } from 'react-router';
import { bindActionCreators } from 'redux';
import Scale from '../../../../../assets/img/icons/scale.svg';
import {
  loadTrialMemberDetails,
  clinicalMemberDetails,
  trails,
  trial,
} from '../../../../../../../ducks/clinical_trials';
import { connect } from 'react-redux';
import NextIcon from '../../../../../../components/icons/next';


interface MatchParams {
  ClinicalTrialID: string;
  WLType: string;
  MemberID: string;
}

interface Props extends RouteComponentProps<MatchParams> {
  loadTrialMemberDetails: Function,
  trial: any
}

class Slider extends Component<Props,
  {}> {


  private move(dir: number) {
    let { WLType, ClinicalTrialID } = this.props.match.params;
    let memberId;
    const items = this._items();
    for (let i = 0; i < items.length; i++) {
      if (items[i].MemberID === this.props.match.params.MemberID) {
        const next = items[i + dir];
        memberId = next.MemberID;
        if (next.WatchListID !== items[i].WatchListID) {
          WLType = this.props.trial.tabs.filter((el: any) => el.WatchListID === next.WatchListID)[0].WLType;
        }
        break;
      }
    }
    this.props.history.push(`/clinical-trials/${ClinicalTrialID}/${WLType}/${memberId}/criteria-details`);
    this.props.loadTrialMemberDetails(ClinicalTrialID, memberId);
  }

  private _items() {
    return this.props.trial.patients.filter((el: any) => el.MemberID && el.WLType === this.props.match.params.WLType);
  }

  render() {
    const prev = '<';
    const next = '>';
    let curentName;
    let hasPrev = false;
    let hasNext = false;
    let index = 0;
    const items = this._items();
    for (let i = 0; i < items.length; i++) {
      hasNext = i < items.length - 1;
      index = i + 1;
      if (items[i].MemberID === this.props.match.params.MemberID) {
        curentName = items[i].MemberFullName;
        break;
      }
      hasPrev = true;
    }
    return (
      <div className={'d-flex j-end col-12 details-slider'}>
        <div className={'slider-container  d-flex a-c'}>
          <span onClick={() => this.move(-1)} className={`${hasPrev ? '' : 'disabled'} control`}>
             <NextIcon isNext={false}/>
          </span>
          <span className={'name'}> {curentName}</span>
          <span> {index}/{items.length}</span>
          <span onClick={() => this.move(1)} className={`${hasNext ? '' : 'disabled'} control`}>
           <NextIcon isNext={true}/>
           </span>
        </div>
      </div>
    );
  }
}

const RouteSlider = withRouter(Slider);
const mapStateToProps = (state: any) => ({
  items: trails(state),
  trial: trial(state),
  clinicalMemberDetails: clinicalMemberDetails(state),
});

const mapDispatchToProps = (dispatch: any) => (
  bindActionCreators({
    loadTrialMemberDetails,
  }, dispatch)
);
export default connect(mapStateToProps, mapDispatchToProps)(RouteSlider);
