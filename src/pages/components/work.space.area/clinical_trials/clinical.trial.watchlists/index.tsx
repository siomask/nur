import React, { Component,Fragment } from 'react';
import './index.scss';
import { connect } from 'react-redux';
import {loadAll,loadTrialMembers}from '../../../../../ducks/clinical_trials';

import { bindActionCreators } from 'redux';
import { Route, RouteComponentProps, Redirect, withRouter } from 'react-router-dom';
import ClinicalTrialWatchListCriteriaDetails from './clinical.trial.watchlist.criteria.details';
import ClinicalTrialWatchList from './clinical.trial.watchlist';


interface MatchParams {
  ClinicalTrialID: string;
  MemberID: string;
}

interface Props extends RouteComponentProps<MatchParams> {
  loadAll: Function,
  loadTrialMemberDetails: Function,
  loadTrialMembers: Function,
  items: Array<any>,
  trial: any
}


class ClinicalTrialWatchLists extends Component<Props,
  {}> {


  async componentDidMount() {
    const { ClinicalTrialID  } = this.props.match.params;
    await this.props.loadAll();
    await this.props.loadTrialMembers(ClinicalTrialID);

  }



  render() {


    return (
      <Fragment>
        <Route path='/clinical-trials/:ClinicalTrialID/:WLType/:MemberID/criteria-details' component={ClinicalTrialWatchListCriteriaDetails} exact/>
        <Route path='/clinical-trials/:ClinicalTrialID' component={ClinicalTrialWatchList} exact/>

      </Fragment>
    );
  }
}



const mapDispatchToProps = (dispatch: any) => (
  bindActionCreators({
    loadTrialMembers ,
    loadAll ,
  }, dispatch)
);

const RouteClinicalTrialWatchLists = withRouter(ClinicalTrialWatchLists);

export default connect(null, mapDispatchToProps)(RouteClinicalTrialWatchLists);
