import React, { Component } from 'react';
import './index.scss';
import { connect } from 'react-redux';
import ModuleHeader from '../../../module.header';
import Card from '../../../card';
import { trails, loadAll } from '../../../../../ducks/clinical_trials';
import information from '../../../../../assets/img/icons/information_1.svg';

import { bindActionCreators } from 'redux';
import { NavLink } from 'react-router-dom';
import ReactSVG from 'react-svg';

class ClinicalTrialAbout extends Component<{ loadAll: Function, items: any },
  {}> {
  componentDidMount(): void {
    this.props.loadAll();
  }

  render() {

    const breadCrumbs = [{ name: 'Clinical Trials' }];
    return (
      <section className={'about'}>
        <ModuleHeader
          title={'Clinical Trials'}
          breadcrumbs={breadCrumbs}
        />
        <div className={'row mt'}>

          <div className={'col-12'} style={{ padding: '0 10px' }}>
            {
              this.props.items.map((item: any) => (
                <div className={'clinic-card'} key={item.Name}>
                  <Card title={(<NavLink className={'link fullWidth'}
                                         to={`/clinical-trials/${item.ClinicalTrialID}`}>
                    <span className={'d-flex j-btw'}>
                      <span>{item.Name}&nbsp;</span>
                      {/*<ReactSVG src={information} svgClassName={'info'}/>*/}
                    </span>
                  </NavLink>)}>

                    <div className={'d-flex wrap'}>
                      <div className={'d-flex  col-5 f-col'}>
                        <div className={'row a-c  card-item-row d-flex j-start col-12'}>
                          <span className={'card-label'}>NCT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                          <div className={'card-label-info d-flex a-c'}>
                            <a target={'_blank'} className={'  __link'}
                               href={item.ClinicalTrialDoc.NCTURL}>{item.ClinicalTrialDoc.NCT}</a>
                          </div>
                        </div>
                        <div className={'row a-c  card-item-row d-flex j-start col-12'}>
                          <span className={'card-label'}>Status&nbsp;&nbsp;&nbsp; </span>
                          <div className={'card-label-info d-flex a-c'}>
                            {item.ClinicalTrialDoc.Status}
                          </div>
                        </div>
                      </div>
                      <div className={'d-flex  col-3 '}>

                        <div className={'row a-c  card-item-row d-flex j-btw f-col col-12'}>
                          <span className={'card-label'}>Candidates</span>
                          <span className={'card-label-info'}>{item.CountCandidates || 0}</span>

                        </div>
                        <div className={'row a-c  card-item-row d-flex j-btw f-col  col-12'}>

                          <span className={'card-label'}>Enrolled</span>
                          <div className={'card-label-info d-flex a-c'}>
                            {item.CountEnrolled || 0}/{item.ClinicalTrialDoc.EnrollementEstimationHealthInstitute}
                          </div>

                        </div>
                      </div>


                    </div>
                  </Card>
                </div>
              ))
            }

          </div>
        </div>
      </section>
    );
  }
}


const mapStateToProps = (state: any) => ({
  items: trails(state),
});


const mapDispatchToProps = (dispatch: any) => (
  bindActionCreators({
    loadAll,
  }, dispatch)
);
export default connect(mapStateToProps, mapDispatchToProps)(ClinicalTrialAbout);
