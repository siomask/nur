import React, { Component } from 'react';
import './index.scss';

export class Tabs extends Component<{ defaultActiveTabIndex: any, className: string }, { activeTabIndex: any }> {

  constructor(props: any, context: any) {
    super(props, context);
    this.state = {
      activeTabIndex: this.props.defaultActiveTabIndex,
    };
    this.handleTabClick = this.handleTabClick.bind(this);
  }

  handleTabClick(tabIndex: any) {

    this.setState({
      activeTabIndex: tabIndex === this.state.activeTabIndex ? this.props.defaultActiveTabIndex : tabIndex,
    });
  }

  // Encapsulate <Tabs/> component API as props for <Tab/> children
  renderChildrenWithTabsApiAsProps() {
    return React.Children.map(this.props.children, (child: any, index) => {
      return React.cloneElement(child, {
        onClick: this.handleTabClick,
        tabIndex: index,
        isActive: index === this.state.activeTabIndex,
      });
    });
  }

  // Render current active tab content
  renderActiveTabContent() {
    const children: any = this.props.children;
    const activeTabIndex: any = this.state.activeTabIndex || 0;
    const active: any = children[activeTabIndex];
    if (active) {
      return active.props.children;
    }
  }

  render() {
    return (
      <div className={`tabs tab-set ${this.props.className}`}>
        <ul className="tabs-nav nav navbar-nav navbar-left nav-tabs">
          {this.renderChildrenWithTabsApiAsProps()}
        </ul>
        <div className="tabs-active-content tab-content">
          {this.renderActiveTabContent()}
        </div>
      </div>
    );
  }
};
export const Tab = (props: any) => {
  return (
    <li className={`tab ${props.isActive ? 'active' : ''}`}>
      <a className={`tab-link ${props.linkClassName} ${props.isActive ? 'active' : ''} btn`}
         onClick={(event) => {
           event.preventDefault();
           props.onClick(props.tabIndex);
         }}>
        {props.title}
      </a>
    </li>
  );
};

export default Tabs;
