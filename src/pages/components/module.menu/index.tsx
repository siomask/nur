import React, { Component } from 'react';
import './index.scss';
import { NavLink } from 'react-router-dom';

class ModuleMenu extends Component<{menuItems:any},
  {}> {

  render() {
    const menuItems:any = this.props.menuItems;
    return (
      <section className={'overview-menu'}>
        {
          menuItems.map((item:any, index:any) => (
            <NavLink to={item.route} key={item.title} activeClassName="active">
              <div className={`menu-item d-flex menu-icon-${index} a-c`}>
                <div className={`menu-icon `}>{item.icon}</div>
                <span className={'menu-item-title'}>{item.title}</span>
              </div>
            </NavLink>
          ))
        }
      </section>
    );
  }
}


export default ModuleMenu;
