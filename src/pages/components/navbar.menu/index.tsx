import React, { Component } from 'react';
import { Link, NavLink } from 'react-router-dom';
import './index.scss';
import Logo from '../../../assets/img/logo.png';

import Arrow from '../../../assets/img/icons/down-arrow.svg';
import justice from '../../../assets/img/icons/justice.svg';
import research from '../../../assets/img/new_icons/research.svg';
import folder from '../../../assets/img/new_icons/folder.svg';

import ReactSVG from 'react-svg';
import { connect } from 'react-redux';
import { userSelector } from '../../../ducks/auth';
import IconButton from '@material-ui/core/IconButton';
import { Icon } from '@material-ui/core';

class NavBarMenu extends Component<{ menu: any, user: any, hide: any }, {}> {

  render() {
    const { menu } = this.props;

    return (
      <aside className={'aside'}>

        <div className={'main-navbar'}>
          <div>
            <IconButton>
              <Icon>menu</Icon>
            </IconButton>
          </div>

          <div className={'logo'}>
            <Link to={'/'}> <img src={Logo}/></Link>
          </div>
        </div>

        <div className={'list-navbar-menu'}>
          {
            this.props.hide ? (
              <div className={'hide-area'}></div>
            ) : null
          }
          {
            menu.map((menuItem: any) => {
              let icon;
              if (menuItem.appId === 1) {

                icon = justice;
              } else if (menuItem.appId === 2) {

                icon = folder;
              } else if (menuItem.appId === 3) {
                icon = research;
              }
              return (
                <div key={menuItem.name}>
                  <div className={`menu-item d-flex j-start fixed  ${menuItem.className}`}>
                    {
                      icon && <ReactSVG src={icon}/>
                    }

                    <span>{menuItem.name}</span>
                  </div>
                  <div>
                    {
                      menuItem.menus.map((item: any, index: number) => {
                        return (
                          <NavLink to={item.route} key={item.title}
                                   activeClassName="active"
                                   className={menuItem.className}>
                            <div className={`menu-item d-flex j-btw  `}>
                              <div className={`  d-flex j-start ${item.className}`}>
                                <div className={'d-flex a-c '}>
                                  <span className={'menu-item-title'}>
                                    {
                                      item.title
                                    }
                                  </span>
                                </div>
                              </div>
                            </div>
                          </NavLink>
                        );
                      })
                    }
                  </div>
                </div>
              );
            })
          }
        </div>
      </aside>
    );
  }
}


const mapStateToProps = (state: any) => ({
  user: userSelector(state),
});

export default connect(mapStateToProps)(NavBarMenu);
