import React, { Component } from 'react';

import './index.scss';
import Logo from '../../../assets/img/logo.png';

class NavBarItem extends Component<{}, {}> {

  render() {
    return (
      <aside className={'aside'}>

       <div className={'main-navbar'}>
         <div className={'logo'}>
           <img src={Logo}/>
         </div>
       </div>
        <div className={'list-navbar'}>

        </div>
      </aside>
    );
  }
}


export default NavBarItem;
