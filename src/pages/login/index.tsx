import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { signIn, moduleName } from '../../ducks/auth';
import AVA from '../../assets/img/Group 2.png';
import Ellipse_5 from '../../assets/img/icons/Ellipse_5.png';
import Logo from '../../assets/img/logo.png';
import './login.scss';
import Popup from 'reactjs-popup';

class Login extends Component<{ signIn: any, authError: boolean },
  {}> {

  state = {
    error: false,
    email: '',
    password: '',
  };


  onInput = (e: any) => {
    this.setState({
      [e.target.name]: e.target.value,
      error: false,
    });
  };
  onSubmit = async (e: any) => {
    e.preventDefault();
    e.stopPropagation();
    const { email, password } = this.state;
    if (email && password) {
      try {
        await this.props.signIn(this.state);
      } catch (e) {
        this.setState({
          error: 'Error! Either user or password are wrong. Please try again',
        });
      }

    }
  };

  render() {
    const { authError } = this.props;
    return (
      <div className={'login-page d-flex'}>
        <div className={'col-6  '}>
          <div className={'d-flex a-c fullHeight '}>
            <div className={'d-flex a-c f-col form-container'}>
              <div className={'login-title fullWidth'}>Welcome <span>Patient 360</span></div>

              <form className={'login-form-body fullWidth'}>
                <div className={'form-item d-flex f-col '}>
                  <label>Username</label>
                  <input name={'email'} id={'userName'} value={this.state.email} onChange={this.onInput}
                         required/>
                </div>
                <div className={'form-item d-flex f-col'}>
                  <label>Password</label>
                  <input name={'password'} id={'userPassword'} value={this.state.password}
                         onChange={this.onInput}
                         type={'password'}
                         required/>
                </div>


                <br/>
                <div className={'form-actions d-flex a-c j-start'}>
                  <button type={'submit'} className={'my-btn my-success'} onClick={this.onSubmit}>Sign in</button>
                  {/*<button type={'submit'} className={'my-btn my-default'}>Register</button>*/}

                  <Popup trigger={<a href={'#'}>Forgot Password</a>} modal>
                    {(close: any) => (
                      <div className="modal">
                        <a className="close" onClick={close}>
                          &times;
                        </a>
                        <div className="header"> Forgot Password</div>
                        <div className="content d-flex j-center">
                          Forgot password functionality haven’t implemented yet
                        </div>
                        <div className="actions d-flex j-center">
                          <button
                            className={`d-flex a-c my-btn my-primary  `}
                            onClick={() => {
                              close();
                            }}
                          >
                            close
                          </button>
                        </div>
                      </div>
                    )}
                  </Popup>
                </div>
              </form>
              {
                authError && (<p style={{ color: 'red' }} className={'error-message'}>
                  Error! Either user or password are wrong. Please try again
                </p>)
              }
              {/*<div className={'fullWidth new-here'}>New to Patient 360? <a href={'#'}>Create an account</a>.</div>*/}

            </div>

          </div>
          <div className={'fullWidth terms'}>
            Terms of Use &nbsp;&nbsp;|&nbsp;&nbsp;    Privacy Policy &nbsp;&nbsp;   |   &nbsp;&nbsp; Contact Us
          </div>
        </div>
        <div className={'col-6'}>
          <div className={'d-flex a-c fullHeight fullWidth'}>
            <div className={'d-flex a-c fullHeight j-center help-area fullWidth'}>

              <img src={AVA} style={{ height: '80vh' }}/>
            </div>
          </div>
        </div>

        <div className={'main-logo'}>
          <img src={Ellipse_5}/>
          <img src={Logo}/>
        </div>
      </div>

    );
  }
}

const mapStateToProps = (state: any) => ({
  authError: state[moduleName].error,
});

const mapDispatchToProps = (dispatch: any) => (
  bindActionCreators({
    signIn,
  }, dispatch)
);
export default connect(mapStateToProps, mapDispatchToProps)(Login);
