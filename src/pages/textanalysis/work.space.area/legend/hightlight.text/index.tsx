import React, { Component } from 'react';
import './index.scss';

function findAll(regexPattern: string, sourceString: string) {
  let output = [];
  let match;
  // make sure the pattern has the global flag
  let regexPatternWithGlobal = RegExp(regexPattern, 'gi');
  while (match = regexPatternWithGlobal.exec(sourceString)) {
    // get rid of the string copy
    delete match.input;
    // store the match data
    output.push(match);
  }
  return output;
}

class TextHightLight extends Component<{ item: any }, {}> {

  private hightlight() {

    const yellow = '#D6C636';
    const Orange = '#EA7E2C';
    const { Text, Dictionaries, ModuleConfiguration } = this.props.item;
    let text = Text;
    let KEYWORDS: any = [];
    ModuleConfiguration.Trials.forEach((el: any) => {
      el.Criteria.forEach((criteria: any) => {
        if (criteria.Configuration && criteria.Configuration.ConditionKeyWords) KEYWORDS.push(...criteria.Configuration.ConditionKeyWords);
      });
    });
    Dictionaries.forEach((dictionary: any) => {
      dictionary.Terms.forEach((term: any) => {
        if (KEYWORDS.indexOf(term.Term) > -1 || KEYWORDS.indexOf(term.Abr) > -1) return;

        [
          'Term','Abr'
        ].forEach((el:any)=>{
          let _term = term[el];
          if (!_term) return;
          _term = _term.trim();
          const replace = '_'.repeat(_term.length);
          const _all  = findAll(_term, text);
          _all.forEach((el: any) => {
            const prevCharacter = text[el.index - 1];
            if (!prevCharacter || !prevCharacter.match(/[A-Za-z0-9]+/g)) {
              const beforeClip = text.substr(0, el.index);
              const afterClip = text.substr(el.index + _term.length);
              text = beforeClip + replace + afterClip;
            }
          });
          let re = new RegExp(replace, 'gi');
          text = text.replace(re, `<span style="background-color:${yellow}" title="${term['Term']}">${_term}</span>`);
        })

      });
    });
    KEYWORDS.forEach((_text: any) => {
      let _term = _text;
      if (!_term) return;
      _term = _term.trim();
      const replace = '_'.repeat(_term.length);
      const _all  = findAll(_term, text);
      _all.forEach((el: any) => {
        const prevCharacter = text[el.index - 1];
        if (!prevCharacter || !prevCharacter.match(/[A-Za-z0-9]+/g)) {
          const beforeClip = text.substr(0, el.index);
          const afterClip = text.substr(el.index + _term.length);
          text = beforeClip + replace + afterClip;
        }
      });
      let re = new RegExp(replace, 'gi');
      text = text.replace(re, `<span style="background-color:${Orange}">${_term}</span>`);
    });

    return {
      __html: `<span >${text}</span>`,
    };
  }

  render() {
    return (
      <pre className={'highlight-text'} contentEditable dangerouslySetInnerHTML={this.hightlight()}></pre>
    );
  }
}


export default TextHightLight;
