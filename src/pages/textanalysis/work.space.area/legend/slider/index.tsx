import React, { Component } from 'react';
import './index.scss';
import NextIcon from '../../../../components/icons/next';


interface Props {
  next: Function,
  items: any
}

class Slider extends Component<Props,
  { currentIndex: number }> {

  constructor(p: any) {
    super(p);
    this.state = {
      currentIndex: 0,
    };
  }

  private move(dir: number) {
    this.setState({
      currentIndex: this.state.currentIndex + dir,
    });
    this.props.next(this.state.currentIndex + dir);
  }

  private _items() {
    return this.props.items;
  }

  render() {
    const prev = '<';
    const next = '>';
    const { currentIndex } = this.state;
    let hasPrev = this.state.currentIndex > 0;
    let hasNext = this.state.currentIndex < this._items().length - 1;


    return (
      <div className={'d-flex j-center col-12 details-slider-text'}>
        <div className={'slider-container d-flex a-c'}>
          <span onClick={() => this.move(-1)} className={`${hasPrev ? '' : 'disabled'} control`}>  <NextIcon isNext={false}/> </span>
          <span className={'name'}> Text {this._items().length ? currentIndex + 1 : currentIndex}/{this._items().length}</span>
          <span onClick={() => this.move(1)} className={`${hasNext ? '' : 'disabled'} control`}>  <NextIcon isNext={true}/> </span>
        </div>
      </div>
    );
  }
}


export default Slider;
