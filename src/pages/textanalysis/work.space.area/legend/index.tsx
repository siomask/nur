import React, { Component } from 'react';

import './index.scss';
import { bindActionCreators } from 'redux';
import { loadAll, textAnalysis, textAnalysisComments,textAnalysisDetails,patient, loadPatient } from '../../../../ducks/text_analysis';
import { connect } from 'react-redux';
import Slider from './slider';
import TextHightLight from './hightlight.text';
import { RouteComponentProps, withRouter } from 'react-router-dom';
import Popup from 'reactjs-popup';
import moment from 'moment';

interface MatchParams {
  ClinicalTrialID: string;
  ConditionID: string;
  MemberID: string;
}

interface Props extends RouteComponentProps<MatchParams> {
  patient: any,
  details: any,
  comments: any,
  items: any,
  loadPatient: Function,
  loadAll: Function
}

class TextLegend extends Component<Props, { index: any }> {
  constructor(p: any) {
    super(p);
    this.state = {
      index: 0,
    };
  }

  componentDidMount(): void {
    const patientId = this.props.match.params.MemberID;//location.href.split('/')[location.href.split('/').length - 1];
    this.props.loadPatient(patientId);
    this.props.loadAll(patientId,this.props.match.params.ClinicalTrialID);
  }

  onNext(index: any) {

    this.setState({
      index,
    });
  }

  render() {
    const details = this.props.details;
    const list = this.props.items;
    const item = list[this.state.index];
    const Conditions: any = [];
    const Keywords: any = [];
    const items = this.props.items;
    if (items && items.length) {
      const { ModuleConfiguration } = items[0];
      ModuleConfiguration.Trials.forEach((el: any) => {
        el.Criteria.forEach((criteria: any) => {
          if (criteria.Configuration && criteria.Configuration.ConditionKeyWords) Keywords.push(...criteria.Configuration.ConditionKeyWords);
          if (criteria.OriginalText) Conditions.push(criteria.OriginalText);
        });
      });
    }

    return (
      <div className={'text-legend col-12'}>
        <div className={'d-flex a-c'}>
          <div className={'text-legend col-6 d-flex a-c'}>
            <div className={' col-6 text-center '}>
              {
                this.props.patient ? `${this.props.patient.FirstName} ${this.props.patient.LastName}` : ''
              }
            </div>
            <div className={' col-6 '}>
              <Slider items={list} next={(item: any) => this.onNext(item)}/>
            </div>

          </div>
          <div className={'d-flex j-btw col-6 list-btns'}>
            {/*<Popup trigger={<button className={`d-flex a-c my-btn my-primary  `}>Show Condition </button>} modal>
              {(close: any) => (
                <div className="modal">
                  <a className="close" onClick={close}>
                    &times;
                  </a>
                  <div className="header"><h3 className={'m-0'}> Condition</h3></div>
                  <div className="content">
                    <span>

                      <ul>
                      {
                        details
                          .filter((e: any) => parseInt(e.ConditionID) === parseInt(this.props.match.params.ConditionID))
                          .map((el: any) => (
                            <li key={el.Display}>{el.Display}</li>
                          ))
                      }
                        <li key={Conditions[2]}>{Conditions[2]}</li>
                      </ul>

                    </span>
                  </div>
                  <div className="actions">
                    <button
                      className={`d-flex a-c my-btn my-primary  `}
                      onClick={() => {
                        close();
                      }}
                    >
                      close
                    </button>
                  </div>
                </div>
              )}
            </Popup>*/}
            <Popup trigger={<button className={`d-flex a-c my-btn my-primary  `}>Show Keywords</button>} modal>
              {(close: any) => (
                <div className="modal">
                  <a className="close" onClick={close}>
                    &times;
                  </a>
                  <div className="header"><h3 className={'m-0'}>Keywords</h3></div>
                  <div className="content">
                    <span>
                      {
                        Keywords.join(', ')
                      }
                    </span>
                  </div>
                  <div className="actions">
                    <button
                      className={`d-flex a-c my-btn my-primary  `}
                      onClick={() => {
                        close();
                      }}
                    >
                      close
                    </button>
                  </div>
                </div>
              )}
            </Popup>
            {/*<button className={`d-flex a-c my-btn my-primary  `}>Dictionary</button>*/}

            <Popup trigger={<button className={`d-flex a-c my-btn my-primary  `}>Show Comments</button>} modal>
              {(close: any) => (
                <div className="modal">
                  <a className="close" onClick={close}>
                    &times;
                  </a>
                  <div className="header"><h3 className={'m-0'}> Comments</h3></div>
                  <div className="content">
                    <span>

                      <ul>
                      {
                        this.props.comments.map((el: any) => {
                          const date = moment(el.InsertDate).format('YYYY-MMM-DD hh:mm');
                          return (
                            <li key={el.InsertDate}>
                              <div className={'flex a-c'} style={{ padding: 10 }}>
                                <p className={'flex a-c'}>
                                  <h4>Comment:</h4>
                                  <span>{el.Text}</span>
                                </p>
                                <p className={'flex a-c'}>
                                  <h4>Date:</h4>
                                  <span>{date}</span>
                                </p>
                              </div>
                            </li>
                          );
                        })
                      }
                      </ul>

                    </span>
                  </div>
                  <div className="actions">
                    <button
                      className={`d-flex a-c my-btn my-primary  `}
                      onClick={() => {
                        close();
                      }}
                    >
                      close
                    </button>
                  </div>
                </div>
              )}
            </Popup>
            <Popup trigger={<button className={`d-flex a-c my-btn my-primary  `}>Show Criteria</button>} modal>
              {(close: any) => (
                <div className="modal">
                  <a className="close" onClick={close}>
                    &times;
                  </a>
                  <div className="header"><h3 className={'m-0'}>Criteria</h3></div>
                  <div className="content">
                    <span>

                      <ul>
                      {
                        details
                          .filter((e: any) => parseInt(e.ConditionID) === parseInt(this.props.match.params.ConditionID))
                          .map((el: any) => (
                            <li key={el.OriginalText}>{el.OriginalText}</li>
                          ))
                      }
                      </ul>

                    </span>
                  </div>
                  <div className="actions">
                    <button
                      className={`d-flex a-c my-btn my-primary  `}
                      onClick={() => {
                        close();
                      }}
                    >
                      close
                    </button>
                  </div>
                </div>
              )}
            </Popup>
          </div>
        </div>

        {
          item ? <TextHightLight item={item}/> : null
        }

      </div>
    );
  }
}

const mapStateToProps = (state: any) => ({
  items: textAnalysis(state),
  details: textAnalysisDetails(state),
  comments: textAnalysisComments(state),
  patient: patient(state),
});

const mapDispatchToProps = (dispatch: any) => (
  bindActionCreators({
    loadPatient,
    loadAll,
  }, dispatch)
);
export default connect(mapStateToProps, mapDispatchToProps)(withRouter(TextLegend));

