import React, { Component } from 'react';
import { Route, Switch } from 'react-router';
import {
    Redirect,
} from 'react-router-dom';

import './index.scss';
import TextLegend from './legend';

class TextWorkSpaceArea extends Component<{}, {}> {


  render() {
    return (
      <div className={'space-area'}>
        <Switch>
          <Route exact path="/" render={() => (<Redirect to="/text-analysis/legend"/>)}/>
          <Route path='/text-analysis/legend/:ClinicalTrialID/:MemberID/:ConditionID' component={TextLegend}/>
        </Switch>
      </div>
    );
  }
}


export default TextWorkSpaceArea;
