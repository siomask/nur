import React, { Component } from 'react';
import './index.scss';
import NavBar from '../components/navbar';
import HeaderSearch from '../components/header/search.area';
import TextWorkSpaceArea from './work.space.area';
import Popup from 'reactjs-popup';
import { textAnalysis } from '../../ducks/text_analysis';
import { connect } from 'react-redux';
import NavBarMenu from '../components/navbar.menu';
import ReactSVG from 'react-svg';
import Settings from '../../assets/img/icons/settings.svg';
import HeaderMenu from '../components/header/menu';


class TextAnalysisPage extends Component<{ items: any },
  { condition: any }> {

  constructor(p: any) {
    super(p);
    this.state = {
      condition: false,
    };
  }

  private toggleModal(type: any) {
    this.setState({
      condition: !this.state.condition,
    });
  }

  private closeModal(type: any) {
    this.setState({
      condition: false,
    });
  }

  render() {


    const menu = [
      {
        className: 'text-analysis text-disabled',
        name: 'Text Analysis - Legend',
        menus: [
          {
            className: 'key-words',
            title: 'Keyword',
            route: '/text-analysis/keyword',
          },
          {
            className: 'conditions',
            title: 'Condition Keywords',
            route: '/text-analysis/condition-keywords',
          },/*,
          {
            title: 'Text structure',
            route: '/text-analysis/text-structure',
          },
          {
            title: 'Medication',
            route: '/text-analysis/medication',
          },
          {
            title: 'Typo',
            route: '/text-analysis/typo',
          },
          {
            title: 'Negative Word',
            route: '/text-analysis/negative-word',
          },
          {
            title: 'Keyword Related Number',
            route: '/text-analysis/keyword-rlated-number',
          },
          {
            title: 'Dates',
            route: '/text-analysis/dates',
          },
          {
            title: 'Local Search',
            route: '/text-analysis/local-search',
          },*/
        ],
      },
    ];

    return (
      <div className={'main-layout  d-flex'}>
        <NavBarMenu hide menu={menu}/>
        <main className={'part-container  col-12 d-flex'} style={{ 'paddingTop': 25}}>

            <div className={'col-11'}>
              <div className={'d-flex a-c '}>
                <div className={'col-8'}>
                  <HeaderSearch showPlaceHolder showRefresh={false}/>
                </div>
              </div>
              <TextWorkSpaceArea/>
            </div>
            <div className={'col-1'}>
              <div className={'header-info fullWidth'}>
                <div className={'d-flex j-end a-c'}>
                  <ReactSVG svgClassName={'settings'} src={Settings}/>
                  <span className={'separator'}></span>
                  <HeaderMenu/>
                </div>
              </div>
            </div>
        </main>
      </div>
    );
  }
}


const mapStateToProps = (state: any) => ({
  items: textAnalysis(state),
});

export default connect(mapStateToProps)(TextAnalysisPage);
