import React, { Component } from 'react';
import './index.scss';
import WorkSpaceArea from '../components/work.space.area';

import { userSelector } from '../../ducks/auth';
import NavBarMenu from '../components/navbar.menu';
import { connect } from 'react-redux';


class HomePage extends Component<{ user: any },
  {}> {


  render() {
    const menu = [
      {
        appId: 1,
        name: 'Clinical Trials',
        menus: [
          {
            title: 'Clinical Trials',
            route: '/clinical-trials',
          },
          // {
          //   title: 'Watchlist',
          //   route: '/watchlist',
          // },
          // {
          //   title: 'Recent Activity ',
          //   route: '/recent-activity',
          // },
        ],
      },
      {
        appId: 2,
        name: 'Patient Profile',
        menus: [
          {
            title: 'About',
            route: '/about',
          },
          {
            title: 'Vital Signs',
            route: '/vital-signs',
          },
          {
            title: 'Family History',
            route: '/family-history',
          },
          /* {
             title: 'Calculators',
             route: '/calculators',
           },
           {
             title: 'Timeline',
             route: '/timeline',
           },*/
          {
            title: 'Medications',
            route: '/medications',
          },
          {
            title: 'Allergies',
            route: '/allergies',
          },
          {
            title: 'Lab Results',
            route: '/lab-results',
          },
          /* {
             title: 'Tasks',
             route: '/tasks',
           },*/
        ],
      },
      {
        appId: 3,
        name: 'Epidemiology',
        menus: [
          {
            title: 'All Incidents',
            route: '/All-Incidents',
          },
          {
            title: 'Recently Opened',
            route: '/Recently-Opened',
          },
          {
            title: 'Recently Closed',
            route: '/Recently-Closed',
          },
        ],
      },

    ].filter((el: any) => {
      return this.props.user.info.app.map((e: any) => e.ApplicationID).indexOf(el.appId) > -1;
    });
    return (
      <div className={'main-layout d-flex'}>
        <NavBarMenu hide={false} menu={menu}/>
        <main className={'part-container'}>
          <WorkSpaceArea/>
        </main>
      </div>
    );
  }
}

const mapStateToProps = (state: any) => ({
  user: userSelector(state),
});

export default connect(mapStateToProps)(HomePage);
