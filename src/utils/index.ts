import { Map } from 'immutable';

const _Map: any = Map;
export const arrayToMap = (arr: Array<any>, DataModule: any, key: string = 'ClinicalTrialID') => {
  return arr.reduce((
    acc: any, item: any) => acc.set(item[key], DataModule ? new DataModule(item) : item),
    new _Map({}),
  );
};
