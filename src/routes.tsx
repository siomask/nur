import React, { Component } from 'react';
import { Switch, BrowserRouter, Route, Redirect } from 'react-router-dom';

import HomePage from './pages/home';
import TextAnalysisPage from './pages/textanalysis';
import Login from './pages/login/index';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { loadUser, userSelector, moduleName } from './ducks/auth';

import { ConnectedRouter } from 'react-router-redux';
import history from './history';

class AppRoutes extends Component<{ user: any, isChecked: boolean, loadUser: any },
  {}> {

  componentDidMount(): void {
    this.props.loadUser();
  }

  render() {
    const { user, isChecked } = this.props;

    if (!isChecked) return null;
    const loader: any = document.getElementById('loader');
    if (loader) loader.style.display = 'none';
    return (
      <BrowserRouter>
        {
          user ? (
              <Switch>
                <Route path='/text-analysis' component={TextAnalysisPage}/>
                <Route path='/' component={HomePage}/>

                <Route exact path="*" render={() => (
                  <Redirect to="/"/>
                )}/>
              </Switch>
            ) :
            (
              <Switch>
                <Route path='/' component={Login}/>
                <Route exact path="*" render={() => (
                  <Redirect to="/"/>
                )}/>
              </Switch>
            )
        }
      </BrowserRouter>
    );
  }
}

const mapDispatchToProps = (dispatch: any) => (
  bindActionCreators({
    loadUser,
  }, dispatch)
);
const mapStateToProps = (state: any) => ({
  user: userSelector(state),
  isChecked: state[moduleName].isChecked,
});
export default connect(mapStateToProps, mapDispatchToProps)(AppRoutes);

