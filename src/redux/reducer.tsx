import { combineReducers } from 'redux';
import authReducer, { moduleName as authModule } from '../ducks/auth';
import clinical_trialsReducer, { moduleName as clinical_trialsModule } from '../ducks/clinical_trials';
import text_analysisReducer, { moduleName as text_analysisModule } from '../ducks/text_analysis';
import EPIDEMIOLOGY_reducer, { _moduleName as EPIDEMIOLOGY } from '../ducks/EPIDEMIOLOGY';


export default combineReducers({
  [authModule]: authReducer,
  [clinical_trialsModule]: clinical_trialsReducer,
  [text_analysisModule]: text_analysisReducer,
  [EPIDEMIOLOGY]: EPIDEMIOLOGY_reducer,
});
