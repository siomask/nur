import {saga as authSaga} from '../ducks/auth'
import {saga as clinical_trials} from '../ducks/clinical_trials'
import {saga as text_analysis} from '../ducks/text_analysis'
import {saga as EPIDEMIOLOGY} from '../ducks/EPIDEMIOLOGY'
import {all} from 'redux-saga/effects'

export default function * rootSaga() {
    yield all([
        EPIDEMIOLOGY(),
        authSaga(),
        clinical_trials(),
        text_analysis(),
    ])
}
