export interface Item {
  names: string[];
  description: string;
  color: string;
  SNOMED?: any;
}

export interface Data {
  [key: string]: Item;
}

export const text = `Histological diagnosis of cardiac sarcoidosis: Endomyocardinal biopsy specimens with non-caseating epithelioid CAB granulomas and no alternative cause identified. Clinical diagnosis of probable cardiac sarcoidosis. Histologic diagnosis of extracaridac sarcoidosis and one or more of the following is present while reasonable alternative MI cardiac causes other than CS have been excluded: Corticosteroid or immunosuppressive therapy responsive cardiomyopathy or heart block. Unexplained reduced LVEF (<40%). Mobitz type two second degree heart block or third degree heart block Depressed left vanticular ejection fraction < 50% Patchy uptake on cardiac PDG-PET in pattern consistent with cardiac sarcoidosis. Late gadolinium enhancement (LGE) on cardiac magnetic Myocardial infraction resonance imaging in pattern consistent with CS. Positive gallium uptake in a pattern consistent with CS (cardiac sarcoidosis)`;

export const highlights = ['other than', 'heart'];

