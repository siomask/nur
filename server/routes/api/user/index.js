const router = require('express').Router();


router.get('/user', function(req, res) {

  res.json({
    status: true,
    data: req.user,
  });

});


module.exports = router;
