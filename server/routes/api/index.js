const router = require("express").Router();
const request = require('request');

router.all("/", function (req, res) {
  res.json({
    status: true,
    message: "Info about api",
    user: req.user
  });
});

router.use("/users", require("./user"));
router.use("/patient/:id", function(req, res) {

  request(`http://hapi.fhir.org/baseDstu3/Patient/${req.params.id}?_pretty=true`, function (error, response, body) {
    res.json(JSON.parse(body));
  });
});

module.exports = router;
