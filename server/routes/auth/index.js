const passport = require("passport");
const router = require("express").Router();
var jwt = require("jwt-simple");
const async = require("async");

var config = require("../../config");

const User = require("../../models/user");

router.all("/", function (req, res, next) {
  res.json({
    status: true,
    message: "Info about auth."
  });
});

router.post("/login", function (req, res, next) {
  var passportCtrl = function () {
    passport.authenticate("local", function (err, user, info) {
      if (err) {
        return res.status(400).json({
          status: false,
          message: err.message || "Something bad."
        });
      }

      if (!user) {
        return res.status(400).json({
          status: false,
          message: info.message || "Email or password is incorrect."
        });
      }

      req.login(user, function (err) {
        if (err) {
          return res.status(400).json({
            status: false,
            message: err.message || "Something bad."
          });
        }

        res.json({
          status: true,
          message: "Login success.",
          token: user.token,
          user: user
        });
      });
    })(req, res, next);
  };

  if ((req.body.email === config.superadmin.email) && (req.body.password === config.superadmin.password)) {
    User.findOne({email: req.body.email}, function (err, user) {
      if (err) {
        return res.status(400).json({
          status: false,
          message: err.message || "Something bad."
        });
      } else if (user) {
        passportCtrl();
      } else {

        new User({
          email: config.superadmin.email,
          password: config.superadmin.password,
          firstName: "David",
          secondName: "Cohen",
          gender: "male",
          role: config.USER_ROLE.SUPER,
          parent: req.user ? req.user._id : null,
          "birthDate": "1919-05-07"
        }).save(function (err, user) {

          if (err) {
            return res.status(400).json({
              status: false,
              message: err.message || "Something bad."
            });
          }

          user.token = "JWT " + jwt.encode({_id: user._id, email: user.email}, config.security.secret);
          user.save(function (err, user) {
            if (err) {
              return res.status(400).json({
                status: false,
                message: err.message || "Something bad."
              });
            }

            passportCtrl();
          });

        });
      }
    });

  } else {
    passportCtrl();
  }

});

router.get("/logout", function (req, res) {
  if (!req.isAuthenticated()) {
    return res.status(400).json({
      status: false,
      message: "You are not logged in."
    });
  }

  req.user.token = "";

  req.user.save(function () {
    req.logout();

    res.json({
      status: true,
      message: "Logout success."
    });
  });
});

module.exports = router;
