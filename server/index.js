const path = require('path');
const http = require('http');
const morgan = require('morgan');
const express = require('express');
const passport = require('passport');
const session = require('express-session');
const busboy = require('connect-busboy');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');

const fs = require('fs');
const https = require('https');

const proxy = require('express-http-proxy');
const MongoStore = require('connect-mongo')(session);

const mongoose = require('./middleware/mongoose');

const config = require('./config');
const routes = require('./routes');
var cors = require('cors');

const app = express();
app.use(cors());
// const credentials = {
//   key: fs.readFileSync('/home/ec2-user/oxivisuals/server/oxi.key'),
//   ca: fs.readFileSync('/home/ec2-user/oxivisuals/server/doormap_app.ca-bundle'),
//   cert: fs.readFileSync('/home/ec2-user/oxivisuals/server/doormap_app.crt')
// };
const server = http.createServer(app);
// const server = https.createServer(credentials, app);


app.use(morgan('dev'));

app.use(busboy());

app.use(cookieParser());

app.use(bodyParser.urlencoded({
  limit: '50mb',
  extended: false,
}));

app.use(bodyParser.json({ limit: '50mb' }));

var expireTime = 1000 * 60 * 60 * 24 * 3;//3 days
app.use(session({
  secret: config.security.secret,
  resave: true,
  saveUninitialized: true,
  cookie: { expires: new Date(Date.now() + expireTime), maxAge: expireTime },
  store: new MongoStore({
    mongooseConnection: mongoose.connection,
  }),
}));

app.use(passport.initialize());
app.use(passport.session());

app.use(express.static(path.join(__dirname, '../dist')));
app.use(express.static(path.join(__dirname, '../build')));
app.use('/', routes);

app.use('*', function(req, res) {

  res.sendFile(path.join(__dirname, '../build/index.html'));
});

app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    console.error(err.message, err);
    res.render('error', {
      message: 'Bad request',
      error: 'bad',
    });
  });
}

app.use(function(err, req, res, next) {
  res.status(err.status || 500);

  res.render('error', {
    message: err.message,
    error: {},
  });
});

server.on('listening', function() {
  var addr = server.address();
  var bind = typeof addr === 'string' ? 'pipe ' + addr : 'port ' + addr.port;

  console.log('Listening on ' + bind);
});

server.on('error', function(error) {
  if (error.syscall !== 'listen') {
    throw error;
  }

  var bind = typeof config.port === 'string' ? 'Pipe ' + config.port : 'Port ' + config.port;

  switch (error.code) {
    case 'EACCES':
      console.error(bind + ' requires elevated privileges');
      process.exit(1);
      break;
    case 'EADDRINUSE':
      console.error(bind + ' is already in use');
      process.exit(1);
      break;
    default:
      throw error;
  }
});

server.listen(config.port, function(er) {
  console.log('Express server listening on port ', er);
});


require('./middleware/passport')(passport);
